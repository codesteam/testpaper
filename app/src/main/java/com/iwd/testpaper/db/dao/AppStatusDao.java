package com.iwd.testpaper.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.iwd.testpaper.BuildConfig;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.utility.Print;


/**
 * Created by water on 12/27/16.
 */

public class AppStatusDao extends BaseDao {
    public static final String PUBLISHED = "published";
    public static final String APP_FIRST_RUN = "app_first_run";
    public static final String APP_FIRST_DOWNLOAD_COMPLETE = "app_first_download_complete";
    public static final String LAST_RESPONDED_TIME_PARTS_WITH_DETIALS = "last_responded_time_parts_with_details";
    public static final String LAST_RESPONDED_TIME_ADVERTISE = "last_responded_time_advertise";
    public static final String LAST_RESPONDED_TIME_PARTS = "last_responded_time_parts";
    public static final String LAST_RESPONDED_TIME_BOARDS = "last_responded_time_boards";
    public static final String LAST_RESPONDED_TIME_YEARS = "last_responded_time_years";
    public static final String LAST_RESPONDED_TIME_SUBJECTS = "last_responded_time_subjects";
    public static final String LAST_RESPONDED_TIME_DAILY_EXAM = CommonConstants.TAG_DAILY_EXAM;
    public static final String LAST_RESPONDED_TIME_MODEL_TEST = CommonConstants.TAG_MODEL_TEST;
    public static final String LAST_RESPONDED_TIME_SUGGESTION = CommonConstants.TAG_SUGGESTION;
    public static final String LAST_RESPONDED_TIME_PBQ = CommonConstants.TAG_PBQ;
    public static final String LAST_RESPONDED_TIME_PACKAGE = CommonConstants.TAG_PACKAGE;
    public static final String LAST_RESPONDED_TIME_EXAM_HISTORY = CommonConstants.TAG_EXAM_HISTORY;
    public static final String LAST_RESPONDED_TIME_NOTICE_BOARD = CommonConstants.TAG_NOTICE_BOARD;

    public static String TABLE_NAME = "table_app_status";

    public static String CREATE =  "create table table_app_status " +
            "(id integer primary key NOT NULL, " +
            "app_first_run integer," +
            "app_first_download_complete integer," +
            LAST_RESPONDED_TIME_PARTS_WITH_DETIALS+" text," +
            LAST_RESPONDED_TIME_PARTS+" text," +
            LAST_RESPONDED_TIME_DAILY_EXAM+" text," +
            LAST_RESPONDED_TIME_MODEL_TEST+" text," +
            LAST_RESPONDED_TIME_SUGGESTION+" text," +
            LAST_RESPONDED_TIME_PBQ+" text," +
            LAST_RESPONDED_TIME_PACKAGE+" text," +
            LAST_RESPONDED_TIME_EXAM_HISTORY+" text," +
            LAST_RESPONDED_TIME_BOARDS+" text," +
            LAST_RESPONDED_TIME_YEARS+" text," +
            LAST_RESPONDED_TIME_SUBJECTS+" text," +
            LAST_RESPONDED_TIME_NOTICE_BOARD+" text," +
            LAST_RESPONDED_TIME_ADVERTISE+" text," +
            "published integer)"; // 0 FOR FIRST TIME RUN

    public AppStatusDao(Context context) {
      super(context);

    }

    public void create(){ // single row created from CommonApplication
        int rowcount = getRowCount();
        if (rowcount<=0){
            Print.e(this, "Row count App Status: "+ rowcount);
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, 0);
            contentValues.put(APP_FIRST_DOWNLOAD_COMPLETE, 0);
            contentValues.put(PUBLISHED,  BuildConfig.system_index);
            contentValues.put(LAST_RESPONDED_TIME_PARTS_WITH_DETIALS, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_ADVERTISE, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_PARTS, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_DAILY_EXAM, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_MODEL_TEST, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_PBQ, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_SUGGESTION, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_PACKAGE, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_EXAM_HISTORY, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_NOTICE_BOARD, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_BOARDS, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_YEARS, CommonConstants.START_TIME_STAMP);
            contentValues.put(LAST_RESPONDED_TIME_SUBJECTS, CommonConstants.START_TIME_STAMP);
            contentValues.put(APP_FIRST_RUN, 0);
            try{
                database.insert(TABLE_NAME, null, contentValues);
                Print.e(this, "Insertion App Status Complete");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private int getRowCount() // if return 0 first time run
    {
        String q = "select * from "+TABLE_NAME;
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();
        return res.getCount();
    }

    public int getStatus(String field)
    {
        String q = "select * from "+TABLE_NAME+" limit 1";
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();

        int status = 0;
        while(res.isAfterLast() == false){
            status = res.getInt(res.getColumnIndex(field));
            res.moveToNext();
        }
        return status;
    }
    public String getStatusString(String field)
    {
        String q = "select * from "+TABLE_NAME+" limit 1";
        Cursor res =  database.rawQuery( q, null );
        res.moveToFirst();
        String status = null;
        while(res.isAfterLast() == false){
            status = res.getString(res.getColumnIndex(field));
            res.moveToNext();
        }
        Print.e(this, "get "+field+" : "+status);
        return status;
    }

    public void update(String field, Long value){
        try {
            ContentValues cv = new ContentValues();
            cv.put(field,value);
            database.update(TABLE_NAME, cv, "id = 0", null);
            Print.e(this, "Update status: "+field);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public void update(String field, String value){
        try {
            ContentValues cv = new ContentValues();
            cv.put(field,value);
            database.update(TABLE_NAME, cv, "id = 0", null);
            Print.e(this, "Update status: "+field);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
