package com.iwd.testpaper.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iwd.testpaper.retrofit.responces.parts.Result;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;

public class PartsDao extends BaseDao {

    public static String TABLE_NAME= "parts";
    public static String TITLE = "title";
    public static String IMAGE = "image";
    public static String TAG = "tag";
    public static String PARENT_ID = "parent_id";
    public static String ORDER = "order_";
    public static String LAST_MODIFIED = "last_modified";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            ORDER+" integer, "+
            PARENT_ID+" integer, "+
            LAST_MODIFIED+" text, "+
            TAG+" text NOT NULL UNIQUE, "+
            IMAGE+" text, "+
            TITLE+" text NOT NULL"+");";

    public static String ORDER_BY = " ORDER BY "+ORDER+" ASC";
    public PartsDao(Context context) {
        super(context);
    }


    public void insert(Result result){
        Print.e(this, "entry insertion start: ");
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(PARENT_ID, result.getParent());
        contentValues.put(TITLE, result.getTitle());
        contentValues.put(TAG, result.getTag());
        contentValues.put(ORDER, result.getOrder());
        contentValues.put(LAST_MODIFIED, result.getModifiedAt());
        contentValues.put(IMAGE, result.getImage());
            try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public long update(Result result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PARENT_ID, result.getParent());
            contentValues.put(TITLE, result.getTitle());
            contentValues.put(TAG, result.getTag());
            contentValues.put(ORDER, result.getOrder());
            contentValues.put(LAST_MODIFIED, result.getModifiedAt());
            contentValues.put(IMAGE, result.getImage());
            //contentValues.put(TAG, result.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {result.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Parts: "+result.getTitle());
            return 0;
        }
    }


    public ArrayList<Result> getParents(){
        ArrayList<Result> examtypes = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME+" where "+PARENT_ID+" IS NULL" +ORDER_BY, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Result examtype = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            examtype.setId(id);
            examtype.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            examtype.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
            examtype.setTag(cursor.getString(cursor.getColumnIndex(TAG)));
            examtypes.add(examtype);
            cursor.moveToNext();
        }
        return examtypes;
    }
    public ArrayList<Result> getChilds(long parentId){
        ArrayList<Result> examtypes = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PARENT_ID+"="+parentId+" "+ORDER_BY, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Result examtype = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            examtype.setId(id);
            examtype.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            examtype.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
            examtype.setTag(cursor.getString(cursor.getColumnIndex(TAG)));
            examtypes.add(examtype);
            cursor.moveToNext();
        }
        return examtypes;
    }

    public int count(){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }

    public int childCount(long parentId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+PARENT_ID+"="+parentId+" "+ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
