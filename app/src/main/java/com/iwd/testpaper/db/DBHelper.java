package com.iwd.testpaper.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.iwd.testpaper.R;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.BoardDao;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.db.dao.PartStatusDao;
import com.iwd.testpaper.db.dao.ParticipationDao;
import com.iwd.testpaper.db.dao.PartsDao;
import com.iwd.testpaper.db.dao.QuestionSetDao;
import com.iwd.testpaper.db.dao.SubjectDao;
import com.iwd.testpaper.db.dao.UserDao;
import com.iwd.testpaper.db.dao.YearDao;
import com.iwd.testpaper.retrofit.responces.participation.Participation;
import com.iwd.testpaper.utility.Print;

/**
 * Created by water on 3/19/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper instance = null;

    public static synchronized DBHelper getHelper(Context context) {
        if (instance == null)
            instance = new DBHelper(context);
        return instance;
    }

    public static String getDatabseName(Context context){
        return context.getString(R.string.app_db_name)+".db";
    }
    private DBHelper(Context context)
    {
        super(context, getDatabseName(context), null, 1);
    }
    @Override
    public void onOpen(SQLiteDatabase db){
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(AppStatusDao.CREATE);
        db.execSQL(PartsDao.CREATE);
        db.execSQL(QuestionSetDao.CREATE);
        db.execSQL(ParticipationDao.CREATE);
        db.execSQL(PartStatusDao.CREATE);
        db.execSQL(UserDao.CREATE);
        db.execSQL(BoardDao.CREATE);
        db.execSQL(YearDao.CREATE);
        db.execSQL(SubjectDao.CREATE);
        db.execSQL(ExamHistoriyDao.CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
       db.execSQL("DROP TABLE IF EXISTS "+AppStatusDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ QuestionSetDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ParticipationDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ PartsDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ PartStatusDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ UserDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ BoardDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ YearDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ SubjectDao.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+ ExamHistoriyDao.TABLE_NAME);
        onCreate(db);
    }
}