package com.iwd.testpaper.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iwd.testpaper.retrofit.responces.subject.Result;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class SubjectDao extends BaseDao {

    public static final String Title = "title";
    public static String TABLE_NAME = "table_subjects";
    public static String CREATE =  "create table "+TABLE_NAME+" " +
            "(id integer primary key NOT NULL, " +
            Title +" text)";

    public SubjectDao(Context context) {
      super(context);
    }
    public void insert(Result result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(Title, result.getTitle());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Result result){
        try {
            ContentValues cv = new ContentValues();
            cv.put(Title,result.getTitle());
            database.update(TABLE_NAME, cv, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Result> getResult(){
        ArrayList<Result> boards = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME , null );
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
          Result board = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            board.setId(id);
            board.setTitle(cursor.getString(cursor.getColumnIndex(Title)));
            boards.add(board);
            cursor.moveToNext();
        }
        return boards;
    }
}
