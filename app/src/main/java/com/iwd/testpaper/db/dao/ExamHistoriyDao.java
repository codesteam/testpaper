package com.iwd.testpaper.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.iwd.testpaper.retrofit.responces.examhistory.Result;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;


/**
 * Created by water on 12/27/16.
 */

public class ExamHistoriyDao extends BaseDao {

    public static String TABLE_NAME= "exam_history";
    public static String TITLE = "title";
    public static String MARK = "image";
    public static String USER_ID = "userid";
    public static String EXAM_ID = "examid";
    public static String STATUS = "status";
    public static String LAST_MODIFIED = "last_modified";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            LAST_MODIFIED+" integer, "+
            STATUS+" integer, "+
            USER_ID+" integer NOT NULL, "+
            EXAM_ID+" integer NOT NULL, "+
            MARK+" text, "+
            TITLE+" text"+");";

    public static String ORDER_BY = " ORDER BY "+LAST_MODIFIED+" ASC";
    public ExamHistoriyDao(Context context) {
      super(context);
    }
    public void insert(Result result){

        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, result.getId());
        contentValues.put(LAST_MODIFIED, result.getModifiedAt());
        contentValues.put(STATUS, result.getStatus());
        contentValues.put(USER_ID, result.getUser());
        contentValues.put(EXAM_ID, result.getExam());
        contentValues.put(MARK, result.getMark());
        contentValues.put(TITLE, result.getTitle());
        try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(result);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update(Result result){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LAST_MODIFIED, result.getModifiedAt());
            contentValues.put(STATUS, result.getStatus());
            contentValues.put(USER_ID, result.getUser());
            contentValues.put(EXAM_ID, result.getExam());
            contentValues.put(MARK, result.getMark());
            contentValues.put(TITLE, result.getTitle());
            database.update(TABLE_NAME, contentValues, ID+" = '"+result.getId()+"'", null);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public ArrayList<Result> getResult(Long userId){
        ArrayList<Result> histories = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+USER_ID+"="+userId +" "+ORDER_BY, null );
        cursor.moveToFirst();
        Print.e(this, "history count: "+cursor.getCount() +" :user id:"+userId);
        while(cursor.isAfterLast() == false){
          Result history = new Result();
          long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            history.setId(id);
            history.setModifiedAt(cursor.getLong(cursor.getColumnIndex(LAST_MODIFIED)));
            history.setStatus(cursor.getInt(cursor.getColumnIndex(STATUS)));
            history.setUser(cursor.getLong(cursor.getColumnIndex(USER_ID)));
            history.setExam(cursor.getLong(cursor.getColumnIndex(EXAM_ID)));
            history.setMark(cursor.getFloat(cursor.getColumnIndex(MARK)));
            history.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            histories.add(history);
            cursor.moveToNext();
        }
        return histories;
    }

    public int historyCount(Long userId, Long examId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+USER_ID+"="+userId +" and "+EXAM_ID+"="+examId, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }

    public int historyCount(Long userId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+USER_ID+"="+userId, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
