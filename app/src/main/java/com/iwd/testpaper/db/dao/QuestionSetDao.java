package com.iwd.testpaper.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.iwd.testpaper.retrofit.responces.participation.QuestionSet;
import com.iwd.testpaper.retrofit.responces.participation.Result;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;

public class QuestionSetDao extends BaseDao {

    public static String TABLE_NAME= "qset";
    public static String TITLE = "title";
    public static String IMAGE = "image";
    public static String PRICE = "price";
    public static String PART_ID = "part_id";
    public static String PARTICAPTION_ID = "particaption_id";
    public static String ORDER = "order_";
    public static String LAST_MODIFIED = "last_modified";
    public static String FIRST_LETTER = "first_letter";
    public static String TIME = "time";
    public static String CREATE="create table "+TABLE_NAME +"("+
            ID+" integer primary key NOT NULL, "+
            ORDER+" integer, "+
            TIME+" integer, "+
            PARTICAPTION_ID +" integer, "+
            PART_ID+" integer, "+
            LAST_MODIFIED+" text, "+
            PRICE+" text, "+
            IMAGE+" text, "+
            FIRST_LETTER+" text, "+
            TITLE+" text NOT NULL, "+
            "FOREIGN KEY("+PARTICAPTION_ID+") REFERENCES "+ParticipationDao.TABLE_NAME+"("+ParticipationDao.ID+") ON DELETE CASCADE"+");";

    public static String ORDER_BY = " ORDER BY "+ORDER+" ASC";
    public QuestionSetDao(Context context) {
        super(context);
    }


    public void insert(QuestionSet questionSet, long particaptionId, long partType){
        Print.e(this, "entry insertion start: ");
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, questionSet.getId());
        contentValues.put(PARTICAPTION_ID, particaptionId);
        contentValues.put(TITLE, questionSet.getTitle());
        contentValues.put(PART_ID, partType);
        contentValues.put(PRICE, questionSet.getPrice());
        contentValues.put(ORDER, questionSet.getOrder());
        contentValues.put(TIME, questionSet.getTime());
        contentValues.put(LAST_MODIFIED, questionSet.getModifiedAt());
        contentValues.put(IMAGE, questionSet.getImage());
        contentValues.put(FIRST_LETTER, questionSet.getFirstLetter());
            try{
            int state = (int) database.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            Print.e(this, "entry insertion state: "+state);
            if (state == -1){
                update(questionSet, particaptionId, partType);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public long update(QuestionSet questionSet, long particaptionId, long partType){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, questionSet.getId());
            contentValues.put(PARTICAPTION_ID, particaptionId);
            contentValues.put(TITLE, questionSet.getTitle());
            contentValues.put(PART_ID, partType);
            contentValues.put(PRICE, questionSet.getPrice());
            contentValues.put(TIME, questionSet.getTime());
            contentValues.put(ORDER, questionSet.getOrder());
            contentValues.put(LAST_MODIFIED, questionSet.getModifiedAt());
            contentValues.put(IMAGE, questionSet.getImage());
            contentValues.put(FIRST_LETTER, questionSet.getFirstLetter());
            //contentValues.put(TAG, result.getTag());
            long update = database.update(TABLE_NAME, contentValues,
                    whereId,
                    new String[] {questionSet.getId()+"" });
            return update;

        }catch (Exception ex){
            ex.printStackTrace();
            Print.e(this, "Failed to update Parts: "+questionSet.getTitle());
            return 0;
        }
    }

    public ArrayList<Result> getQsetAsParticaption(Long particaptionId){
        ArrayList<Result> examtypes = new ArrayList<>();
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+ PARTICAPTION_ID +"="+particaptionId+" "+ORDER_BY, null );
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Result row = new Result();
            long id = (long)cursor.getInt(cursor.getColumnIndex(ID));
            row.setId(id);
            row.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
            row.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
            row.setParticipationType(cursor.getLong(cursor.getColumnIndex(PART_ID)));
            row.setPrice(cursor.getString(cursor.getColumnIndex(PRICE)));
            row.setTime(((long) cursor.getInt(cursor.getColumnIndex(TIME))));
            row.setFirstLetter(cursor.getString(cursor.getColumnIndex(FIRST_LETTER)));
            examtypes.add(row);
            cursor.moveToNext();
            Print.e(this, "found q set: "+id +" "+row.getTitle());

        }
        return examtypes;
    }

    public int count(String particaptionId){
        Cursor cursor =  database.rawQuery( "select * from "+ TABLE_NAME +" where "+ PARTICAPTION_ID +"="+particaptionId+" "+ORDER_BY, null );
        cursor.moveToFirst();
        return cursor.getCount();
    }

}
