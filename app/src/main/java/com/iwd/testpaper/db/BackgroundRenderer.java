package com.iwd.testpaper.db;

public interface BackgroundRenderer {
    public void updateForeground();
}
