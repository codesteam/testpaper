/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.iwd.testpaper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;

import com.iwd.testpaper.R;
import com.iwd.testpaper.db.dao.UserDao;
import com.iwd.testpaper.retrofit.responces.User;


public class EditProfileActivity extends AppCompatActivity{

    

    AppCompatEditText etUserName, etPhone, etPass, etRepeatPass, etEmail;
    AppCompatButton btnSubmit;
    String username, phone, pass, repass, email;
    AppCompatCheckBox cbAgree, cbRememberMe;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);
        etUserName = (AppCompatEditText)findViewById(R.id.et_username);
        etPhone = (AppCompatEditText)findViewById(R.id.et_phone);
        etEmail = (AppCompatEditText)findViewById(R.id.et_email);
        etPass = (AppCompatEditText)findViewById(R.id.et_password);
        etRepeatPass = (AppCompatEditText)findViewById(R.id.et_repeatpassword);
        btnSubmit = (AppCompatButton) findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUserName.getText().toString().trim();
                phone = etPhone.getText().toString().trim();
                pass = etPass.getText().toString();
                repass = etRepeatPass.getText().toString();
                email = etEmail.getText().toString().trim();
               /* if (username == null || username.toString().length() > 0){
                    Toast.makeText(SignUpActivity.this, "Please enter a valid user name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (phone == null || phone.toString().length() > 0){
                    Toast.makeText(SignUpActivity.this, "Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pass == null || pass.toString().length() > 0){
                    Toast.makeText(SignUpActivity.this, "Please enter a valid password", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (repass == null || repass.toString().length() > 0){
                    if(!pass.equals(repass)){
                        Toast.makeText(SignUpActivity.this, "Confirm password does not match", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }*/

                //TODO check if this user is available, if ok then save it to shared pref got to MyAccount

            }
        });
        btnSubmit = (AppCompatButton)findViewById(R.id.btn_submit);
        cbAgree = (AppCompatCheckBox) findViewById(R.id.cbLicenseAgree);
        cbRememberMe = (AppCompatCheckBox) findViewById(R.id.cbRememberMe); // get value of remember me after all api call;
        String checkBoxText = "I agree to all the <a href='"+getString(R.string.terms_condition_url)+"' > Terms and Conditions</a>";

        cbAgree.setText(Html.fromHtml(checkBoxText));
        cbAgree.setMovementMethod(LinkMovementMethod.getInstance());
       // btnSubmit.setEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}