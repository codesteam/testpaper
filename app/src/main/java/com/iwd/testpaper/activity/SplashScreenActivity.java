package com.iwd.testpaper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.iwd.testpaper.R;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.common.CommonSettings;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.DBHelper;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.PartsDao;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.handler.ApiCallManager;
import com.iwd.testpaper.utility.ConnectionDetector;
import com.iwd.testpaper.utility.Print;

public class SplashScreenActivity extends AppCompatActivity implements BackgroundRenderer{
    boolean lock = true;
    private FragmentManager fm = null;
    private NoNet mNoNet;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.logo_first_frame)
            .error(R.drawable.logo_first_frame)
            .priority(Priority.HIGH);
    ImageView logoView;
    Button btnRetry;
    TextView tvNoInternet;
    TextView tvLoading;
    /*SpotsDialog.Builder spotsDialogBuilder;
    android.app.AlertDialog alertDialog;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        logoView = (ImageView) findViewById(R.id.logo);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        tvNoInternet = (TextView) findViewById(R.id.tvNoInternet);
        tvLoading = (TextView) findViewById(R.id.tvLoading);
        //getWindow().setWindowAnimations(3);
        DBHelper.getHelper(this);
       // spotsDialogBuilder = new SpotsDialog.Builder();
        if (!ConnectionDetector.isNetworkPresent(this)){
            PartsDao partsDao = new PartsDao(this);
            if (partsDao.count() == 0){
                fm = getSupportFragmentManager();
                mNoNet = new NoNet(this);
                mNoNet.initNoNet(R.layout.no_net_default, null, this, fm, false);
                mNoNet.setConnectionCallback(new ConnectionCallback() {
                    @Override
                    public void Networkupdate(boolean isConnectionActive) {
                        if(isConnectionActive){
                            startOperation();
                        }
                    }
                });
                mNoNet.RegisterNoNet();
                mNoNet.showDefaultDialog();
            }else{
                CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
            }

        }else{
            startOperation();
        }
        btnStateInactive();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOperation();
            }
        });
    }


    @Override
    protected void onResume() {

        super.onResume();
        Print.e(this, "onResume");
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    private void startOperation(){
        lock = true;
        btnStateInactive();
        new Thread()
        {
            int p =0;
            public void run()
            {
                while (lock){
                    if (p==0){
                        p=1;
                        //Print.e(this, "CallForAppStatusApi 0");
                        ApiCallManager apiCallManager = ApiCallManager.getInstance(SplashScreenActivity.this, getString(R.string.base_url), CommonSettings.getInstance(SplashScreenActivity.this).getApiToken());
                        String timestamp = new AppStatusDao(SplashScreenActivity.this).getStatusString(AppStatusDao.LAST_RESPONDED_TIME_PARTS_WITH_DETIALS);
                        apiCallManager.callInitApis(timestamp, new APIClientResponse() {
                            @Override
                            public void onFailure(String msg, Object sender) {
                                Print.e(this, "failed");

                                lock = false;
                                PartsDao partsDao = new PartsDao(SplashScreenActivity.this);
                                if(partsDao.count() > 0){
                                    //gotoMainActivity();
                                    btnStateActive();
                                }else{
                                    btnStateActive();
                                }
                                if(CommonConstants.dummy_data_active){
                                    CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                                }

                            }

                            @Override
                            public void onSuccess(String msg, Object sender) {
                                Print.e(this, "success");
                                lock = false;
                                CommonConstants.GotoLoginActivity(SplashScreenActivity.this);
                            }
                        }, SplashScreenActivity.this);
                    }
                }
                //do async operations here
                //handler.sendEmptyMessage(0);
            }
        }.start();

    }

    private void btnStateInactive(){
        Glide.with(this)
                .applyDefaultRequestOptions(options)
                .load(R.drawable.logo)
                .into(logoView);
        btnRetry.setTextColor(getResources().getColor(R.color.gray));
        //btnRetry.setBackgroundColor(getResources().getColor(R.color.transparent));
        tvNoInternet.setTextColor(getResources().getColor(R.color.gray));
        tvLoading.setTextColor(getResources().getColor(R.color.black));
        btnRetry.setClickable(false);
    }

    private void btnStateActive(){
        Glide.with(SplashScreenActivity.this)
                .applyDefaultRequestOptions(options)
                .load(R.drawable.logo_first_frame)
                .into(logoView);
        btnRetry.setTextColor(getResources().getColor(R.color.red));
        //btnRetry.setBackgroundColor(getResources().getColor(R.color.red));
        tvNoInternet.setTextColor(getResources().getColor(R.color.black));
        tvLoading.setTextColor(getResources().getColor(R.color.gray));
        btnRetry.setClickable(true);
    }

    @Override
    public void updateForeground() {

    }
}
