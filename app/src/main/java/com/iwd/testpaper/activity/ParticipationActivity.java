/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.iwd.testpaper.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.iwd.testpaper.R;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.common.CommonSettings;
import com.iwd.testpaper.customdialog.LoadingDialog;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.db.dao.PartStatusDao;
import com.iwd.testpaper.db.dao.ParticipationDao;
import com.iwd.testpaper.fragments.ExamHistoryFragment;
import com.iwd.testpaper.fragments.ParticipationFragment;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.handler.ApiCallManager;
import com.iwd.testpaper.utility.Print;
import com.iwd.testpaper.utility.ToastMsg;


public class ParticipationActivity extends AppCompatActivity{
    LoadingDialog.Builder builder;
    String partId;
    String tag;
    String boardId = null;
    String year = null;

    String title;
    String subject = null;
    ParticipationDao participationDao;

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part);
        partId = getIntent().getStringExtra(CommonConstants.EXTRA_PART_ID);
        tag = getIntent().getStringExtra(CommonConstants.EXTRA_TAG);
        boardId = getIntent().getStringExtra(CommonConstants.EXTRA_BOARD_ID);
        year = getIntent().getStringExtra(CommonConstants.EXTRA_YEAR);
        title = getIntent().getStringExtra(CommonConstants.EXTRA_TITLE);
        builder = new LoadingDialog.Builder(ParticipationActivity.this).isCancellable(false);
        builder.build();

        participationDao = new ParticipationDao(this);
        ApiCallManager apiCallManager = ApiCallManager.getInstance(this, getString(R.string.base_url),CommonSettings.getInstance(this).getApiToken());
        Print.e(this, "Parts Activity");
        Print.e(this, "TAg + "+ tag);

        String timestamp = new PartStatusDao(ParticipationActivity.this).getStatus(tag)+"";
        if(tag.equals(CommonConstants.TAG_PACKAGE)){
            apiCallManager.callPackageAPI(timestamp, new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentPackage();
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentPackage();
                }
            }, null);
        }
        else if(tag.equals(CommonConstants.TAG_EXAM_HISTORY)){
            apiCallManager.callExamHistoryAPI(timestamp, CommonSettings.getInstance(ParticipationActivity.this).getUserId()+"", new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentExamHistory();
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentExamHistory();
                }
            }, null);

        }
        else if(tag.equals(CommonConstants.TAG_NOTICE_BOARD)){
            apiCallManager.callNoticBoardAPI(timestamp, new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentNoticeBoard();
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentNoticeBoard();
                }
            }, null);
        }else{
            apiCallManager.callForParticipation(timestamp, boardId, year, subject, partId, new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    builder.dismiss();
                    if (participationDao.count(partId) > 0){ // first check is this participtions already downloaded
                        gotoFragmentParticipation();
                    }else{ // nothing found in database
                        ToastMsg.Toast(ParticipationActivity.this, getString(R.string.please_tray_again_later));
                    }
                    //
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    builder.dismiss();
                    gotoFragmentParticipation();
                }
            }, null);

        }

        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recycler, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }else{
                    super.onBackPressed();
                }
            case R.id.sort_option:
                /*startActivity(new Intent(this, SpruceActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));*/
                break;
            case R.id.recycler_option:
                break;
            case R.id.list_view_option:
                /*startActivity(new Intent(this, ListViewActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));*/
                break;
            default:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void gotoFragmentParticipation(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment recyclerFragment = fm.findFragmentById(R.id.fragment_part);
        if (recyclerFragment == null) {
            recyclerFragment = ParticipationFragment.newInstance();
            recyclerFragment.setArguments(getIntent().getExtras());
            fm.beginTransaction()
                    .replace(R.id.fragment_part, recyclerFragment, recyclerFragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }

    public void gotoFragmentPackage(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment recyclerFragment = fm.findFragmentById(R.id.fragment_part);
        if (recyclerFragment == null) {
            recyclerFragment = ParticipationFragment.newInstance();
            recyclerFragment.setArguments(getIntent().getExtras());
            fm.beginTransaction()
                    .replace(R.id.fragment_part, recyclerFragment, recyclerFragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }
    public void gotoFragmentExamHistory(){
        Bundle bundle = getIntent().getExtras();
        bundle.putString(CommonConstants.EXTRA_USER_ID, CommonSettings.getInstance(ParticipationActivity.this).getUserId()+"");
        FragmentManager fm = getSupportFragmentManager();
        Fragment recyclerFragment = fm.findFragmentById(R.id.fragment_part);
        if (recyclerFragment == null) {
            recyclerFragment = ExamHistoryFragment.newInstance();
            recyclerFragment.setArguments(bundle);
            fm.beginTransaction()
                    .replace(R.id.fragment_part, recyclerFragment, recyclerFragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }
    public void gotoFragmentNoticeBoard(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment recyclerFragment = fm.findFragmentById(R.id.fragment_part);
        if (recyclerFragment == null) {
            recyclerFragment = ParticipationFragment.newInstance();
            recyclerFragment.setArguments(getIntent().getExtras());
            fm.beginTransaction()
                    .replace(R.id.fragment_part, recyclerFragment, recyclerFragment.getClass().getName())
                    .commitAllowingStateLoss();
        }
    }
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        }else{
            super.onBackPressed();
        }
    }
    public String getUserFromDb(){
        return "user.id";
    }
}
