package com.iwd.testpaper.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.iwd.testpaper.R;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.common.CommonSettings;
import com.iwd.testpaper.corouselpagerkk.ExamActivityTemp;
import com.iwd.testpaper.corouselpagerkk.KKViewPager;
import com.iwd.testpaper.corouselpagerkk.TestFragmentAdapter;
import com.iwd.testpaper.customdialog.DownloadCompleteListner;
import com.iwd.testpaper.customdialog.LoadingDialog;
import com.iwd.testpaper.customdialog.LoadingWithProgressBarDialog;
import com.iwd.testpaper.customdialog.RankListnear;
import com.iwd.testpaper.customdialog.ResolutionDialog;
import com.iwd.testpaper.customdialog.ResolutionDialogWithRankOptions;
import com.iwd.testpaper.customdialog.ResolutionListnear;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.DBHelper;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.handler.ApiCallManager;
import com.iwd.testpaper.retrofit.responces.parts.Result;
import com.iwd.testpaper.retrofit.responces.question.Question;
import com.iwd.testpaper.retrofit.responces.submissions.ExamSubmissionsResponce;
import com.iwd.testpaper.retrofit.responces.submitexam.History;
import com.iwd.testpaper.retrofit.responces.submitexam.SubmitExam;
import com.iwd.testpaper.utility.ConnectionDetector;
import com.iwd.testpaper.utility.GlideImageDownloader;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class ExamActivity extends AppCompatActivity implements BackgroundRenderer{
    boolean lock = true;
    private boolean innerAppStatusLock = false;
    private FragmentManager fm = null;
    private NoNet mNoNet;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.logo_first_frame)
            .error(R.drawable.logo_first_frame)
            .priority(Priority.HIGH);
    private KKViewPager mPager;
    String board;
    String year;
    Long participtionId;
    String chapterId;
    String examTitle = "";
    int examTime = 0;
    LoadingWithProgressBarDialog.Builder builder;
    Bundle bundle;
    //Button btnSubmit;
    CountDownTimer countDownTimer;
    int examTimeInMilis;
    TextView tvAnswerd;
    //TextView tvExamTitle;
    //TextView tvTotalMark;
    TextView tvTotalTime;
    int totalQuestion = 0;
    Long examHistoryId;
    int givenRank;
    //TextView tvTotalQuestion;
    //CollapsingToolbarLayout collapsingToolbarLayout;
    /*SpotsDialog.Builder spotsDialogBuilder;
    android.app.AlertDialog alertDialog;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam );
        tvAnswerd = (TextView) findViewById(R.id.tvAnswered);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        mPager = (KKViewPager) findViewById(R.id.kk_pager);
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            if(extras.containsKey(CommonConstants.EXTRA_BOARD_ID)){
                board = extras.getString(CommonConstants.EXTRA_BOARD_ID);
                Print.e(this, "board Id: "+board);
            }
            if(extras.containsKey(CommonConstants.EXTRA_CHAPTER_ID)){
                chapterId = extras.getString(CommonConstants.EXTRA_CHAPTER_ID);
                Print.e(this, "chapterId Id: "+chapterId);
            }
            if(extras.containsKey(CommonConstants.EXTRA_PARTICIPATION_ID)){
                participtionId = extras.getLong(CommonConstants.EXTRA_PARTICIPATION_ID);
                Print.e(this, "EXTRA_PARTICIPATION_ID: "+ participtionId);
            }
            if(extras.containsKey(CommonConstants.EXTRA_YEAR)){
                year = extras.getString(CommonConstants.EXTRA_YEAR);
                Print.e(this, "year: "+year);
            }
            if(extras.containsKey(CommonConstants.EXTRA_EXAM_TITLE)){
                examTitle = extras.getString(CommonConstants.EXTRA_EXAM_TITLE);
                Print.e(this, "examTitle: "+examTitle);
            }
            if(extras.containsKey(CommonConstants.EXTRA_EXAM_TIME)){
                examTime = extras.getInt(CommonConstants.EXTRA_EXAM_TIME);
                Print.e(this, "examTime: "+examTime);
            }
            Print.e(this, "Extra: not null");

        }else{
            Print.e(this, "Extra: "+"null");
        }


       // spotsDialogBuilder = new SpotsDialog.Builder();
        if (!ConnectionDetector.isNetworkPresent(this)){
            fm = getSupportFragmentManager();
            mNoNet = new NoNet(this);
            mNoNet.initNoNet(R.layout.no_net_default_with_back, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            }, this, fm, false);
            mNoNet.setConnectionCallback(new ConnectionCallback() {
                @Override
                public void Networkupdate(boolean isConnectionActive) {
                    Print.e(this, "isConnectionActive: "+isConnectionActive);
                    if(isConnectionActive){
                        //restarting application to open this activity automatically from wifi screen
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
            });
            mNoNet.RegisterNoNet();
            mNoNet.showDefaultDialog();

        }else{
            builder = new LoadingWithProgressBarDialog.Builder(ExamActivity.this, new ResolutionListnear() {
                @Override
                public void onClickRight() {
                    // retry again
                    startOperation();
                }

                @Override
                public void onClickLeft() {
                    //back to previous activity
                    finish();
                }
            }).isCancellable(false);
            builder.build();
            //getWindow().setWindowAnimations(3);
            DBHelper.getHelper(this);
            startOperation();
        }
    }


    @Override
    protected void onResume() {

        super.onResume();
        Print.e(this, "onResume");
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ResolutionDialog.Builder resolutionBuilder;
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        resolutionBuilder = new ResolutionDialog.Builder(this);
        resolutionBuilder.isCancellable(false)
                .setRightBtnText(getString(R.string.back))
                .setLeftBtnText(getString(R.string.quit))
                .setMessage(getString(R.string.quiting_exam))
                .build(new ResolutionListnear() {
            @Override
            public void onClickRight() {
                resolutionBuilder.dismiss();

            }

            @Override
            public void onClickLeft() {
                finish();
            }
        });
    }

    private void badRequestDialog(final String message){
        resolutionBuilder = new ResolutionDialog.Builder(this);
        resolutionBuilder.isCancellable(false)
                .setRightBtnText(getString(R.string.retry))
                .setLeftBtnText(getString(R.string.back))
                .setMessage(message)
                .build(new ResolutionListnear() {
                    @Override
                    public void onClickRight() {
                        resolutionBuilder.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }

                    @Override
                    public void onClickLeft() {
                        finish();
                    }
                });
    }
    private void lowBalance(final String message){
        resolutionBuilder = new ResolutionDialog.Builder(this);
        resolutionBuilder.isCancellable(false)
                .setRightBtnText(getString(R.string.recharge))
                .setLeftBtnText(getString(R.string.back))
                .setMessage(message)
                .build(new ResolutionListnear() {
                    @Override
                    public void onClickRight() {
                        Intent intent = new Intent(ExamActivity.this, MyAccountActivity.class);
                        intent.putExtra(CommonConstants.EXTRA_PARTICIPATION_ID, participtionId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        ExamActivity.this.startActivity(intent);
                        ExamActivity.this.finish();

                    }

                    @Override
                    public void onClickLeft() {
                        finish();
                    }
                });
    }
    private void examHistory(final String message){
        resolutionBuilder = new ResolutionDialog.Builder(this);
        resolutionBuilder.isCancellable(false)
                .setRightBtnText(getString(R.string.exam_history))
                .setLeftBtnText("Back")
                .setMessage(message)
                .build(new ResolutionListnear() {
                    @Override
                    public void onClickRight() {
                        resolutionBuilder.dismiss();
                        Intent intent = new Intent(ExamActivity.this, ExamHistoryActivity.class);
                        intent.putExtra(CommonConstants.EXTRA_PARTICIPATION_ID, participtionId);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        ExamActivity.this.startActivity(intent);
                        ExamActivity.this.finish();

                    }

                    @Override
                    public void onClickLeft() {
                        finish();
                    }
                });
    }

    private void startOperation(){
        ApiCallManager apiCallManager = ApiCallManager.getInstance(ExamActivity.this, getString(R.string.base_url), CommonSettings.getInstance(this). getApiToken());
        String timestamp = new AppStatusDao(ExamActivity.this).getStatusString(AppStatusDao.LAST_RESPONDED_TIME_PARTS_WITH_DETIALS);

        apiCallManager.callForQuestionAPI(timestamp,board, year, String.valueOf(participtionId), new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                Print.e(this, "failed");
                builder.dismiss();
                innerAppStatusLock = false;
                RetrofitError retrofitError = (RetrofitError) sender;
                lock = false;
                String message = ((Question)retrofitError.getBody()).getMessage();
                int status = retrofitError.getResponse().getStatus();
                if (status == CommonConstants.ERROR_CODE_EXAM_PARICIPITED){
                    examHistory(message);
                }else if (status == CommonConstants.ERROR_CODE_PAYMENT_REQUIRED){
                    lowBalance(message);
                }else{
                    badRequestDialog(getString(R.string.please_tray_again_later));
                }
                //builder.modeRetry();
               // downloadImages();
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                Print.e(this, "success");
                innerAppStatusLock = false;
                lock = false;
                Question question = (Question) sender;
                examHistoryId = question.getExamHistoryId();
                ArrayList<String> imgs = new ArrayList<>();
                if (question.getResults()!=null){
                    for(com.iwd.testpaper.retrofit.responces.question.Result r: question.getResults()){
                        if (r.getImage()!=null){
                            imgs.add(r.getImage());
                        }
                    }
                }
                if (imgs.size()>0){
                    downloadImages(imgs, question);
                }else{
                    loadUI(question);
                }

            }
        }, ExamActivity.this);

    }

    ArrayList<GlideImageDownloader> glideImageDownloaders = new ArrayList<>();
    private void downloadImages(final ArrayList<String> imgs, final Question question){
        builder.modePB();
        glideImageDownloaders.clear();

        for(String url: imgs){
            GlideImageDownloader glideImageDownloader = new GlideImageDownloader(url, this);
            glideImageDownloaders.add(glideImageDownloader);
        }

        for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
            glideImageDownloader.setDownloadCompleteListner(new DownloadCompleteListner() {
                @Override
                public void complete() {
                    boolean downloadcom = true;
                    builder.getPb().setProgress(builder.getPb().getProgress()+100/imgs.size());
                    for(GlideImageDownloader glideImageDownloader: glideImageDownloaders){
                        if(!glideImageDownloader.isDownloadComplete()){
                            downloadcom = false;
                            break;
                        }
                    }
                    if(downloadcom){
                        loadUI(question);
                    }

                }
            });

            glideImageDownloader.setImageViewPb(builder.getImageViewPb());
            glideImageDownloader.download();
        }

    }

    void loadUI(Question question){
        builder.dismiss();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                initSpruce();
            }
        };
        totalQuestion = question.getResults().size();
        mPager.setAdapter(new TestFragmentAdapter(getSupportFragmentManager(),
                ExamActivity.this,  (ArrayList<com.iwd.testpaper.retrofit.responces.question.Result>)question.getResults()));
        mPager.setAnimationEnabled(true);
        mPager.setFadeEnabled(true);
        mPager.setFadeFactor(0.6f);
        setAnswerd(0);


        if (examTime == 0){
            examTimeInMilis = 15 * 60*1000;
        }else{
            examTimeInMilis = examTime * 60*1000;
        }

        countDownTimer = new CountDownTimer(examTimeInMilis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int sec = (int) millisUntilFinished/1000;
                int min = sec/60;
                int secLeft = sec%60;
               // collapsingToolbarLayout.setTitle("Time: "+min+":"+secLeft);
                tvTotalTime.setText("Time: "+min+":"+secLeft);
            }

            @Override
            public void onFinish() {

            }
        };

        countDownTimer.start();
    }

    private void initSpruce() {
      /*  spruceAnimator = new Spruce.SpruceBuilder(recyclerView)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(recyclerView, 800),
                        ObjectAnimator.ofFloat(recyclerView, "translationX", -recyclerView.getWidth(), 0f).setDuration(800))
                .start();*/
    }


    @Override
    public void updateForeground() {

    }

    public void setAnswerd(int count) {
        tvAnswerd.setText("Answerd: "+count+"/"+totalQuestion);
    }

    public void moveToNextQuestion(){
        mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
    }
    public void submitExam(ArrayList<com.iwd.testpaper.retrofit.responces.question.Result> results){
           int mark = 0;
           ArrayList<History> histories = new ArrayList<>();
           for(com.iwd.testpaper.retrofit.responces.question.Result result: results){
               if (result.getSelOptionId()!=null){
                   if (result.getSelOptionId().equals(result.getAnswer())){
                       mark++;
                   }
                   History history = new History();
                   history.setOption(result.getSelOptionId());
                   history.setQuestion(result.getId());
                   histories.add(history);
               }

           }
            if (CommonSettings.getInstance(this).getUserId() == CommonConstants.GHUEST_USER_ID){ // guest user just show result, give option for sign up
                resolutionBuilder = new ResolutionDialog.Builder(this);
                resolutionBuilder.isCancellable(false)
                        .setRightBtnText(getString(R.string.signUp))
                        .setLeftBtnText("Back")
                        .setMessage("Congratulations! You scored "+mark+"/"+results.size()+".\n"+"For more free exam please Sign Up!!")
                        .build(new ResolutionListnear() {
                            @Override
                            public void onClickRight() {
                                resolutionBuilder.dismiss();
                                Intent intent = new Intent(ExamActivity.this, SignUpActivity.class);
                                intent.putExtra(CommonConstants.EXTRA_PARTICIPATION_ID, participtionId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                ExamActivity.this.startActivity(intent);
                                ExamActivity.this.finish();

                            }

                            @Override
                            public void onClickLeft() {
                                finish();
                            }
                        });
            }else{// registered user send result to server
               submitResult(mark, histories);
            }
    }

    private void submitResult(final int marks,  final ArrayList<History> histories){
        final SubmitExam submitExam = new SubmitExam();
        submitExam.setMarks(marks);
        submitExam.setHistory(histories);
        submitExam.setUser(CommonSettings.getInstance(this).getUserId());
        submitExam.setExam(participtionId);
        final ResolutionDialogWithRankOptions.Builder resolutionBuilderRo = new ResolutionDialogWithRankOptions.Builder(ExamActivity.this);
        resolutionBuilderRo.isCancellable(false)
                .build(new RankListnear() {
                    @Override
                    public void rank(int value) {
                        givenRank = value;
                        submitExam.setRank(value);
                        resolutionBuilderRo.dismiss();
                        final LoadingDialog.Builder builder = new LoadingDialog.Builder(ExamActivity.this).isCancellable(false);
                        builder.setMessage(getString(R.string.please_wait_your_result_is_processing)).build();
                        ApiCallManager apiCallManager = ApiCallManager.getInstance(ExamActivity.this, getString(R.string.base_url), CommonSettings.getInstance(ExamActivity.this).getApiToken());

                        apiCallManager.submitResult(submitExam, String.valueOf(examHistoryId), new APIClientResponse() {
                            @Override
                            public void onFailure(String msg, Object sender) {
                                Print.e(this, "onFailure ABC");
                                builder.dismiss();
                                resolutionBuilder = new ResolutionDialog.Builder(ExamActivity.this);
                                resolutionBuilder.isCancellable(false)
                                        .setRightBtnText(getString(R.string.submit))
                                        .setLeftBtnText("Back")
                                        .setMessage(getString(R.string.network_error_please_submit_again))
                                        .build(new ResolutionListnear() {
                                            @Override
                                            public void onClickRight() {
                                                resolutionBuilder.dismiss();
                                                submit(submitExam);

                                            }

                                            @Override
                                            public void onClickLeft() {
                                                finish();
                                            }
                                        });
                            }

                            @Override
                            public void onSuccess(String msg, Object sender) {
                                Print.e(this, "onSuccess ABC");
                                final ExamSubmissionsResponce responce = (ExamSubmissionsResponce) sender;
                                builder.dismiss();
                                resolutionBuilder = new ResolutionDialog.Builder(ExamActivity.this);
                                resolutionBuilder.isCancellable(false)
                                        .setRightBtnText(getString(R.string.exam_history))
                                        .setLeftBtnText("Back")
                                        .setMessage("Congratulations! You scored "+responce.getMarks()+"/"+responce.getTotalMarks()+"./n"+"Check on Exam History to access your exam results.")
                                        .build(new ResolutionListnear() {
                                            @Override
                                            public void onClickRight() {
                                                resolutionBuilder.dismiss();
                                                Intent intent = new Intent(ExamActivity.this, DistinctExamHistoryActivity.class);
                                                intent.putExtra(CommonConstants.EXTRA_EXAM_HISTORY_ID, examHistoryId);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                                ExamActivity.this.startActivity(intent);
                                                ExamActivity.this.finish();

                                            }

                                            @Override
                                            public void onClickLeft() {
                                                finish();
                                            }
                                        });

                            }
                        }, null);
                    }
                });

    }
    private void submit(final SubmitExam submitExam){

        final LoadingDialog.Builder builder = new LoadingDialog.Builder(ExamActivity.this).isCancellable(false);
        builder.setMessage(getString(R.string.please_wait_your_result_is_processing)).build();
        ApiCallManager apiCallManager = ApiCallManager.getInstance(ExamActivity.this, getString(R.string.base_url), CommonSettings.getInstance(ExamActivity.this).getApiToken());

        apiCallManager.submitResult(submitExam, String.valueOf(examHistoryId), new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                Print.e(this, "onFailure ABC");
                builder.dismiss();
                resolutionBuilder = new ResolutionDialog.Builder(ExamActivity.this);
                resolutionBuilder.isCancellable(false)
                        .setRightBtnText(getString(R.string.submit))
                        .setLeftBtnText("Back")
                        .setMessage(getString(R.string.network_error_please_submit_again))
                        .build(new ResolutionListnear() {
                            @Override
                            public void onClickRight() {
                                resolutionBuilder.dismiss();
                                submit(submitExam);

                            }

                            @Override
                            public void onClickLeft() {
                                finish();
                            }
                        });
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                Print.e(this, "onSuccess ABC");
                final ExamSubmissionsResponce responce = (ExamSubmissionsResponce) sender;
                builder.dismiss();
                resolutionBuilder = new ResolutionDialog.Builder(ExamActivity.this);
                resolutionBuilder.isCancellable(false)
                        .setRightBtnText(getString(R.string.exam_history))
                        .setLeftBtnText("Back")
                        .setMessage("Congratulations! You scored "+responce.getMarks()+"/"+responce.getTotalMarks()+"./n"+"Check on Exam History to access your exam results.")
                        .build(new ResolutionListnear() {
                            @Override
                            public void onClickRight() {
                                resolutionBuilder.dismiss();
                                Intent intent = new Intent(ExamActivity.this, DistinctExamHistoryActivity.class);
                                intent.putExtra(CommonConstants.EXTRA_EXAM_HISTORY_ID, examHistoryId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                ExamActivity.this.startActivity(intent);
                                ExamActivity.this.finish();

                            }

                            @Override
                            public void onClickLeft() {
                                finish();
                            }
                        });

            }
        }, null);

    }


}
