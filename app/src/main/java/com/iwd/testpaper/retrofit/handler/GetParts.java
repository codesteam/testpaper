package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.PartsDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.parts.Part;
import com.iwd.testpaper.retrofit.responces.parts.Result;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetParts extends APICallHandler<Long> {

	private static GetParts singleton = null;

	private GetParts() {
	}

	public static GetParts getInstance() {
		 singleton = new GetParts();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getParts(args[1], args[2],new Callback<Part>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Part arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_PARTS, arg0.getRespondedAt());
					PartsDao partsDao = new PartsDao(CommonApplication.getAppContext());

					for (Result result: arg0.getResults()){
						partsDao.insert(result);
					/*	if (result.getChildren()!= null ){
							for(Child child: result.getChildren()){
								//Print.e(this, "found childs for: "+result.getTag());
								partsDao.insert(child.getId(), child.getParent(), child.getTitle(),
										child.getTag(), child.getOrder(), child.getModifiedAt(), child.getImage());
							}
						}*/
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, Part arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
