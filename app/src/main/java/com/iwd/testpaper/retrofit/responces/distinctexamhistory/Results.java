
package com.iwd.testpaper.retrofit.responces.distinctexamhistory;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("rating")
    @Expose
    private int rating;
    @SerializedName("user")
    @Expose
    private Long user;
    @SerializedName("marks")
    @Expose
    private Double marks;
    @SerializedName("history")
    @Expose
    private List<History> history = null;
    @SerializedName("exam")
    @Expose
    private Long exam;
    @SerializedName("exam_title")
    @Expose
    private String examTitle;
    @SerializedName("exam_price")
    @Expose
    private Double examPrice;
    @SerializedName("exam_questions")
    @Expose
    private List<ExamQuestion> examQuestions = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public Long getExam() {
        return exam;
    }

    public void setExam(Long exam) {
        this.exam = exam;
    }

    public String getExamTitle() {
        return examTitle;
    }

    public void setExamTitle(String examTitle) {
        this.examTitle = examTitle;
    }

    public Double getExamPrice() {
        return examPrice;
    }

    public void setExamPrice(Double examPrice) {
        this.examPrice = examPrice;
    }

    public List<ExamQuestion> getExamQuestions() {
        return examQuestions;
    }

    public void setExamQuestions(List<ExamQuestion> examQuestions) {
        this.examQuestions = examQuestions;
    }

}
