package com.iwd.testpaper.retrofit.handler;


import android.util.Log;

import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.DummyHelper;
import com.iwd.testpaper.retrofit.responces.question.Question;
import com.iwd.testpaper.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetQuestion extends APICallHandler<Long> {

	private  static GetQuestion singleton = null;

	private GetQuestion() {
	}

	public static GetQuestion getInstance() {
		singleton = new GetQuestion();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getQuestion(args[1], args[2],args[3],args[4], args[5], new Callback<Question>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Question arg0,
								Response arg1) {
				sendSuccessFeedBack(callback, arg0);
			}

		});
	}


	private void sendSuccessFeedBack(APIClientResponse callback, Question arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
