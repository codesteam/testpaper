
package com.iwd.testpaper.retrofit.responces.distinctexamhistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DistinctExamHistory {

    @SerializedName("responded_at")
    @Expose
    private Long respondedAt;
    @SerializedName("results")
    @Expose
    private Results results;

    public Long getRespondedAt() {
        return respondedAt;
    }

    public void setRespondedAt(Long respondedAt) {
        this.respondedAt = respondedAt;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

}
