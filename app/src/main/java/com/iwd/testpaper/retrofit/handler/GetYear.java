package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.BoardDao;
import com.iwd.testpaper.db.dao.YearDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;

import com.iwd.testpaper.retrofit.responces.year.Result;
import com.iwd.testpaper.retrofit.responces.year.Year;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetYear extends APICallHandler<Long> {

	private  static GetYear singleton = null;

	private GetYear() {
	}

	public static GetYear getInstance() {
		singleton = new GetYear();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getYears(args[1], args[2],new Callback<Year>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Year arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_YEARS, arg0.getRespondedAt());
					YearDao dao = new YearDao(CommonApplication.getAppContext());
					for (Result result: arg0.getResults()){
						dao.insert(result);
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});
	}

	private void sendSuccessFeedBack(APIClientResponse callback, Year arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
