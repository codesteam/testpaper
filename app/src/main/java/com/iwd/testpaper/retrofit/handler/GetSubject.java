package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.SubjectDao;
import com.iwd.testpaper.db.dao.YearDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.subject.Subject;
import com.iwd.testpaper.retrofit.responces.subject.Result;
import com.iwd.testpaper.retrofit.responces.year.Year;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetSubject extends APICallHandler<Long> {

	private  static GetSubject singleton = null;

	private GetSubject() {
	}

	public static GetSubject getInstance() {
		singleton = new GetSubject();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getSubjects(args[1], args[2],new Callback<Subject>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Subject arg0,
								Response arg1) {
				if(arg0.getResults() != null){
					AppStatusDao appStatusDao = new AppStatusDao(CommonApplication.getAppContext());
					appStatusDao.update(AppStatusDao.LAST_RESPONDED_TIME_SUBJECTS, arg0.getRespondedAt());
					SubjectDao dao = new SubjectDao(CommonApplication.getAppContext());
					for (Result result: arg0.getResults()){
						dao.insert(result);
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});
	}

	private void sendSuccessFeedBack(APIClientResponse callback, Subject arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
