package com.iwd.testpaper.retrofit.api;


import com.iwd.testpaper.db.BackgroundRenderer;

public abstract class APICallHandler<T> {

	public APIs getAPIs(String endPoint){
		return APIHandler.getApiInterface(endPoint);
	}
	abstract  public void callAPI(final APIClientResponse callback,  BackgroundRenderer backgroundRenderer, String... args);
	//abstract  public void callAPI(final APIClientResponse callback, T arg0, String ep, String path);

}
