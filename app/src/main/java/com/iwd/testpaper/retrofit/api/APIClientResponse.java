package com.iwd.testpaper.retrofit.api;

public interface APIClientResponse {

	public void onFailure(String msg, Object sender);
	public void onSuccess(String msg, Object sender);
}
