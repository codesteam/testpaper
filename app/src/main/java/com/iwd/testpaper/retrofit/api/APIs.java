package com.iwd.testpaper.retrofit.api;




import android.support.v7.util.SortedList;

import com.iwd.testpaper.retrofit.handler.GetQuestion;
import com.iwd.testpaper.retrofit.responces.DummyHelper;
import com.iwd.testpaper.retrofit.responces.LoginResponse;
import com.iwd.testpaper.retrofit.responces.RegistrationResponse;
import com.iwd.testpaper.retrofit.responces.board.Board;
import com.iwd.testpaper.retrofit.responces.distinctexamhistory.DistinctExamHistory;
import com.iwd.testpaper.retrofit.responces.examhistory.ExamHistory;
import com.iwd.testpaper.retrofit.responces.participation.Participation;
import com.iwd.testpaper.retrofit.responces.parts.Part;
import com.iwd.testpaper.retrofit.responces.question.Question;
import com.iwd.testpaper.retrofit.responces.subject.Subject;
import com.iwd.testpaper.retrofit.responces.submissions.ExamSubmissionsResponce;
import com.iwd.testpaper.retrofit.responces.submitexam.SubmitExam;
import com.iwd.testpaper.retrofit.responces.year.Year;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;


public interface APIs {

	//This api call for splash screen
	/*@GET("/api/participationtypes/")
	public void getPartsWithDetails(
			@Query("timestamp") String timestamp,
            @Query("package_name") String packageName,
            Callback<Part> callback);*/
	// this api call for service only
	@GET("/api/board/")
	public void getBoards(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Board> callback);
	@GET("/api/year/")
	public void getYears(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Year> callback);
	@GET("/api/subjects/")
	public void getSubjects(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Subject> callback);
	@GET("/api/participationtypes/")
	public void getParts(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<Part> callback);
	@GET("/api/participations/")
	public void getParticipation(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			@Query("subject") String subject,
			@Query("part_id") String partId,
			Callback<Participation> callback);

	@GET("/api/")
	public void getDailyExam(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getModelTest(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getSuggestion(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getPBQ(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			Callback<DummyHelper> callback);
	@GET("/api/")
	public void getPackage(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/exam-history/")
	public void getExamHistory(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("uid") String userId,
			Callback<ExamHistory> callback);
	@GET("/api/exam-history/{exam_id}/")
	public void getDistinctExamHistory(
			@Path("exam_id") String exam_id,
			@Header("Authorization") String authHeader,
			@Query("uid") String userId,
			Callback<DistinctExamHistory> callback);
	@GET("/api/")
	public void getNoticeBoard(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			Callback<DummyHelper> callback);
	@GET("/api/questions")
	public void getQuestion(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("board") String board,
			@Query("year") String year,
			@Query("q_set") String questionSetId,
			Callback<Question> callback);

	@GET("/api/")
	public void purchase(
			@Query("timestamp") String timestamp,
			@Header("Authorization") String authHeader,
			@Query("product_id") String productId,
			@Query("user_id") String userId,
			Callback<DummyHelper> callback);

	@FormUrlEncoded
	@POST("/rest-auth/registration/")
	void registration(
			@Field("phone_number") String phone_number,
			@Field("first_name") String first_name,
			@Field("email") String email,
			@Field("password") String password,
			@Field("confirm_password") String confirm_password,
			Callback<RegistrationResponse> callback);

	@FormUrlEncoded
	@POST("/rest-auth/login/")
	void login(
			@Field("phone_number") String phone_number,
			@Field("password") String password,
			Callback<LoginResponse> callback);

	@PUT("/api/exam-history/{id}/")
	void submitExam(@Path("id") String id, @Body SubmitExam submitExam, Callback<ExamSubmissionsResponce> callback);

}
