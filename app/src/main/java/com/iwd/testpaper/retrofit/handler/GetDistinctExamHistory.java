package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.distinctexamhistory.DistinctExamHistory;
import com.iwd.testpaper.retrofit.responces.examhistory.ExamHistory;
import com.iwd.testpaper.retrofit.responces.examhistory.Result;
import com.iwd.testpaper.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetDistinctExamHistory extends APICallHandler<Long> {

	private  static GetDistinctExamHistory singleton = null;

	private GetDistinctExamHistory() {
	}

	public static GetDistinctExamHistory getInstance() {
		singleton = new GetDistinctExamHistory();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getDistinctExamHistory(args[1], args[2],args[3],new Callback<DistinctExamHistory>() {

			@Override
			public void failure(RetrofitError arg0) {
				Print.e(this, "failed");
				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(DistinctExamHistory arg0,
								Response arg1) {
				if (callback != null) {
					callback.onSuccess("success", arg0);
				}
			}

		});
	}
}
