
package com.iwd.testpaper.retrofit.responces.submissions;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamQuestion {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("options")
    @Expose
    private List<Option> options = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

}
