
package com.iwd.testpaper.retrofit.responces.submitexam;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitExam {

    @SerializedName("user")
    @Expose
    private Long user;
    @SerializedName("exam")
    @Expose
    private Long exam;
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("marks")
    @Expose
    private float marks;
    @SerializedName("history")
    @Expose
    private List<History> history = null;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public float getMarks() {
        return marks;
    }

    public void setMarks(float marks) {
        this.marks = marks;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getExam() {
        return exam;
    }

    public void setExam(Long exam) {
        this.exam = exam;
    }
}
