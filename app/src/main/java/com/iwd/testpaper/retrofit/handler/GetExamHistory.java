package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.DummyHelper;
import com.iwd.testpaper.retrofit.responces.examhistory.ExamHistory;
import com.iwd.testpaper.retrofit.responces.examhistory.Result;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetExamHistory extends APICallHandler<Long> {

	private  static GetExamHistory singleton = null;

	private GetExamHistory() {
	}

	public static GetExamHistory getInstance() {
		singleton = new GetExamHistory();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getExamHistory(args[1], args[2],args[3],new Callback<ExamHistory>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(ExamHistory arg0,
								Response arg1) {
				if (arg0 != null){
					if (arg0.getResults() != null){
						ExamHistoriyDao examHistoriyDao = new ExamHistoriyDao(CommonApplication.getAppContext());
						for(Result r: arg0.getResults()){
							examHistoriyDao.insert(r);
						}
					}

				}
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, ExamHistory arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
