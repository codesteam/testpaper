package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.ParticipationDao;
import com.iwd.testpaper.db.dao.QuestionSetDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.participation.Participation;
import com.iwd.testpaper.retrofit.responces.participation.QuestionSet;
import com.iwd.testpaper.retrofit.responces.participation.Result;
import com.iwd.testpaper.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetParticipation extends APICallHandler<Long> {

	private  static GetParticipation singleton = null;

	private GetParticipation() {
	}

	public static GetParticipation getInstance() {
		singleton = new GetParticipation();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getParticipation(args[1], args[2],args[3],args[4],args[5],args[6],new Callback<Participation>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(Participation arg0,
								Response arg1) {
				if (arg0!= null){
					if (arg0.getResults() != null){
						ParticipationDao participationDao = new ParticipationDao(CommonApplication.getAppContext());
						QuestionSetDao questionSetDao = new QuestionSetDao(CommonApplication.getAppContext());
						for (Result r: arg0.getResults()) {
							r.setPrice(CommonConstants.DEFAULT_PRICE);
							participationDao.insert(r);
							if (r.getQuestionSets() != null){
								for (QuestionSet q: r.getQuestionSets()){
									questionSetDao.insert(q, r.getId(), r.getParticipationType());
									Print.e(this, "inserting qset "+q.getTitle());
									/*Result result = new Result();
									result.setId(q.getId());
									result.setPrice(q.getPrice());
									result.setFirstLetter(q.getFirstLetter());
									result.setImage(q.getImage());
									result.setCreatedAt(q.getCreatedAt());
									result.setOrder(q.getOrder());
									result.setModifiedAt(q.getModifiedAt());
									result.setTitle(q.getTitle());
									result.setParent(r.getId());
									result.setParticipationType(r.getParticipationType());
									participationDao.insert(result);
									Print.e(this, "convertert qset as partici: "+q.getTitle());*/
								}
							}else{
								Print.e(this, "qset null:"+r.getTitle());
							}
						}
					}
				}
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, Participation arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
