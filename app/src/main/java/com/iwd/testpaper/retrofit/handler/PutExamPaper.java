package com.iwd.testpaper.retrofit.handler;


import android.database.Observable;
import android.widget.Toast;

import com.iwd.testpaper.R;
import com.iwd.testpaper.common.CommonApplication;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.db.dao.SubjectDao;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.subject.Result;
import com.iwd.testpaper.retrofit.responces.subject.Subject;
import com.iwd.testpaper.retrofit.responces.submissions.ExamSubmissionsResponce;
import com.iwd.testpaper.retrofit.responces.submitexam.SubmitExam;
import com.iwd.testpaper.utility.Print;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PutExamPaper extends APICallHandler<Long> {

	private static PutExamPaper singleton;
	SubmitExam submitExam;

	private PutExamPaper(SubmitExam submitExam) {
		this.submitExam = submitExam;
	}

	public static PutExamPaper getInstance(SubmitExam submitExam) {
		singleton = new PutExamPaper(submitExam);
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

         getAPIs(args[0]).submitExam(args[1], submitExam, new Callback<ExamSubmissionsResponce>() {
             @Override
             public void success(ExamSubmissionsResponce examSubmissionsResponce, Response response) {
                 Print.e(this, "abc success");
                 if (callback!=null){
                 	callback.onSuccess("success", examSubmissionsResponce);
				 }
             }

             @Override
             public void failure(RetrofitError error) {
                 Print.e(this, "abc failed");
                 callback.onFailure("failed", error);
             }
         });

	}

	private void sendSuccessFeedBack(APIClientResponse callback, Subject arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
