
package com.iwd.testpaper.retrofit.responces.distinctexamhistory;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamQuestion {

    @SerializedName("answer")
    @Expose
    private Long answer;
    @SerializedName("options")
    @Expose
    private List<Option> options = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("image")
    @Expose
    private String image;
    long selectedOption;

    public Long getAnswer() {
        return answer;
    }

    public void setAnswer(Long answer) {
        this.answer = answer;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setSelectedOption(long selectedOption) {
        this.selectedOption = selectedOption;
    }

    public long getSelectedOption() {
        return selectedOption;
    }
}
