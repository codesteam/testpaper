package com.iwd.testpaper.retrofit.handler;

import android.content.Context;


import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.db.dao.AppStatusDao;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.submitexam.SubmitExam;
import com.iwd.testpaper.utility.ConnectionDetector;
import com.iwd.testpaper.utility.Print;

import java.util.HashMap;

/**
 * Created by water on 5/2/16.
 */
public class ApiCallManager {
    private  static int requestTime = 0;
    Context context;
    String apiKey;
    String endPoint;
    public static boolean lock = false;
    AppStatusDao appStatusDao;
    APIClientResponse callbackParts;
    private static HashMap<String, Boolean> callMap;
    private static ApiCallManager apiCallManager = null;
    public static ApiCallManager getInstance(Context context, String endPoint, String apiKey){
        if (apiCallManager == null){
            apiCallManager = new ApiCallManager(context,endPoint, apiKey);
            callMap = new HashMap<String, Boolean>();
            callMap.put(CommonConstants.TAG_DAILY_EXAM, false);
            callMap.put(CommonConstants.TAG_MODEL_TEST, false);
            callMap.put(CommonConstants.TAG_SUGGESTION, false);
            callMap.put(CommonConstants.TAG_PBQ, false);
            callMap.put(CommonConstants.TAG_PACKAGE, false);
            callMap.put(CommonConstants.TAG_EXAM_HISTORY, false);
            callMap.put(CommonConstants.TAG_NOTICE_BOARD, false);
        }
        apiCallManager.setApiKey(apiKey);
        apiCallManager.setEndPoint(endPoint);
        return  apiCallManager;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    private ApiCallManager(Context context, String endPoint, String apiKey){
        this.context = context;
        this.apiKey = apiKey;
        this.endPoint =endPoint;
        appStatusDao = new AppStatusDao(context);
    }

    public void setCallbackParts(APIClientResponse callbackParts) {
        this.callbackParts = callbackParts;
    }

   /* public int callForParts(final String timestamp){
       *//* if(!ConnectionDetector.isNetworkPresent(context)){
            return 0;
        }*//*
        if (ApiCallManager.lock){
            return -1;
        }
        ApiCallManager.lock = true;
        callPartsWithDetailsAPICall(timestamp, callbackParts);
        return 1;
    }*/

    public void callInitApis(final String timestamp, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        Print.e(this, "callInitApis");
        GetParts.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                callback.onFailure("failed", sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                Print.e(this, "GetBoard");
                GetBoard.getInstance().callAPI(new APIClientResponse() {
                    @Override
                    public void onFailure(String msg, Object sender) {
                        callback.onFailure("failed", sender);
                    }

                    @Override
                    public void onSuccess(String msg, Object sender) {
                        Print.e(this, "GetYear");
                        GetYear.getInstance().callAPI(new APIClientResponse() {
                            @Override
                            public void onFailure(String msg, Object sender) {
                                callback.onFailure("failed", sender);
                            }

                            @Override
                            public void onSuccess(String msg, Object sender) {
                                Print.e(this, "GetSubject");
                                GetSubject.getInstance().callAPI(new APIClientResponse() {
                                    @Override
                                    public void onFailure(String msg, Object sender) {
                                        callback.onFailure("failed", sender);
                                    }

                                    @Override
                                    public void onSuccess(String msg, Object sender) {
                                        Print.e(this, "GetSubject success");
                                        callback.onSuccess("success", sender);
                                    }
                                }, null, endPoint, timestamp, apiKey);
                            }
                        }, null, endPoint, timestamp, apiKey);
                    }
                }, null, endPoint, timestamp, apiKey);
            }
        },backgroundRenderer, endPoint, timestamp, apiKey);
    }
    public void callForPartsAPI(String timestamp, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        GetParts.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                callback.onFailure("failed", sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                callback.onSuccess("success", sender);
            }
        },backgroundRenderer, endPoint, timestamp, apiKey);
    }

    public void submitResult(SubmitExam submitExam, String id, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        PutExamPaper.getInstance(submitExam).callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                callback.onFailure("failed", sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                callback.onSuccess("success", sender);
            }
        },backgroundRenderer, endPoint, id, apiKey);
    }
    public void callForQuestionAPI(String timestamp, String board, String year, String questionSetId, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
       Print.e(this, "apiKey: "+apiKey);
        Print.e(this, "timestamp: "+timestamp);
        GetQuestion.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                callback.onFailure("failed", sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                callback.onSuccess("success", sender);
            }
        },backgroundRenderer, endPoint, timestamp, apiKey, board, year, questionSetId);

    }

    public void callForPurchase(String timestamp, String productId, String userId, final APIClientResponse callback){
        Purchase.getInstance().callAPI(new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                callback.onFailure("failed", sender);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                callback.onSuccess("success", sender);
            }
        },null, endPoint, timestamp, apiKey, productId, userId);

    }
    public void callForParticipation(String timestamp, String board, String year, String subject, String partId, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        if(ConnectionDetector.isNetworkPresent(context)){
            GetParticipation.getInstance().callAPI(new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_PBQ, false);
                    callback.onFailure("failed", sender);
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_PBQ, false);
                    callback.onSuccess("success", sender);
                }
            },backgroundRenderer, endPoint, timestamp, apiKey, board, year, subject, partId);

        }else{
            callback.onFailure("No Internt Connection", null);
        }

    }
    public int callPackageAPI(String timestamp, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        if(apiCallManager.callMap.get(CommonConstants.TAG_PACKAGE)){
            return -1;
        }
        if(ConnectionDetector.isNetworkPresent(context)){
            GetPackage.getInstance().callAPI(new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_PACKAGE, false);
                    callback.onFailure("failed", sender);
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_PACKAGE, false);
                    callback.onSuccess("success", sender);
                }
            },backgroundRenderer, endPoint, timestamp, apiKey);
            apiCallManager.callMap.put(CommonConstants.TAG_PACKAGE, true);
            return 1;
        }else{
            callback.onFailure("No Internt Connection", null);
            return -1;

        }
    }
    public int callExamHistoryAPI(String timestamp, String userId, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        if(apiCallManager.callMap.get(CommonConstants.TAG_EXAM_HISTORY)){
            return -1;
        }
        if(ConnectionDetector.isNetworkPresent(context)){
            GetExamHistory.getInstance().callAPI(new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_EXAM_HISTORY, false);
                    callback.onFailure("failed", sender);
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_EXAM_HISTORY, false);
                    callback.onSuccess("success", sender);
                }
            },backgroundRenderer, endPoint, timestamp, apiKey, userId);
            apiCallManager.callMap.put(CommonConstants.TAG_EXAM_HISTORY, true);
            return 1;
        }else{
            callback.onFailure("No Internt Connection", null);
            return -1;

        }

    }
    public int callDistinctExamHistoryAPI(String historyId, String userId, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        if(ConnectionDetector.isNetworkPresent(context)){
            GetDistinctExamHistory.getInstance().callAPI(new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    callback.onFailure("failed", sender);
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    callback.onSuccess("success", sender);
                }
            },backgroundRenderer, endPoint, historyId, apiKey, userId);

            return 1;
        }else{
            callback.onFailure("No Internt Connection", null);
            return -1;

        }

    }
    public int callNoticBoardAPI(String timestamp, final APIClientResponse callback, BackgroundRenderer backgroundRenderer){
        if(apiCallManager.callMap.get(CommonConstants.TAG_NOTICE_BOARD)){
            return -1;
        }
        if(ConnectionDetector.isNetworkPresent(context)){
            GetNoticeBoard.getInstance().callAPI(new APIClientResponse() {
                @Override
                public void onFailure(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_NOTICE_BOARD, false);
                    callback.onFailure("failed", sender);
                }

                @Override
                public void onSuccess(String msg, Object sender) {
                    apiCallManager.callMap.put(CommonConstants.TAG_NOTICE_BOARD, false);
                    callback.onSuccess("success", sender);
                }
            },backgroundRenderer ,endPoint, timestamp, apiKey);
            apiCallManager.callMap.put(CommonConstants.TAG_NOTICE_BOARD, true);
            return 1;
        }else{
            callback.onFailure("No Internt Connection", null);
            return -1;

        }

    }
}
