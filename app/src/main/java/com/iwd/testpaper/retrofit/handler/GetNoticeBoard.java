package com.iwd.testpaper.retrofit.handler;


import com.iwd.testpaper.db.BackgroundRenderer;
import com.iwd.testpaper.retrofit.api.APICallHandler;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.responces.DummyHelper;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetNoticeBoard extends APICallHandler<Long> {

	private  static GetNoticeBoard singleton = null;

	private GetNoticeBoard() {
	}

	public static GetNoticeBoard getInstance() {
		singleton = new GetNoticeBoard();
		return singleton;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {

		return new CloneNotSupportedException();
	}

	@Override
	public void callAPI(final APIClientResponse callback,final BackgroundRenderer backgroundRenderer, final String... args) {

		getAPIs(args[0]).getNoticeBoard(args[1], args[2],new Callback<DummyHelper>() {

			@Override
			public void failure(RetrofitError arg0) {

				if (callback != null) {
					callback.onFailure("failed", arg0);
				}
			}

			@Override
			public void success(DummyHelper arg0,
								Response arg1) {
				sendSuccessFeedBack(callback, arg0);
			}

		});


	}


	private void sendSuccessFeedBack(APIClientResponse callback, DummyHelper arg0){

		if (callback != null) {
			callback.onSuccess("success", arg0);
		}
	}

}
