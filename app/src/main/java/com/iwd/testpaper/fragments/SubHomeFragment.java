/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.iwd.testpaper.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.GridView;

import com.iwd.testpaper.R;
import com.iwd.testpaper.activity.ParticipationActivity;
import com.iwd.testpaper.adapter.CustomGridAdapter;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.customdialog.YearBoardDialogListener;
import com.iwd.testpaper.customdialog.YearBoardSpinnerDialog;
import com.iwd.testpaper.db.dao.BoardDao;
import com.iwd.testpaper.db.dao.PartsDao;
import com.iwd.testpaper.db.dao.YearDao;
import com.iwd.testpaper.retrofit.responces.parts.Result;
import com.iwd.testpaper.utility.Print;

import java.util.ArrayList;


public class SubHomeFragment extends Fragment {

    public static SubHomeFragment newInstance() {
        return new SubHomeFragment();
    }

    GridView grid;
    ArrayList<Result> parts = new ArrayList<>();
    ArrayList<com.iwd.testpaper.retrofit.responces.board.Result> boards = new ArrayList<>();
    ArrayList<com.iwd.testpaper.retrofit.responces.year.Result> years = new ArrayList<>();
    PartsDao partsDao;
    Long partId;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_home, container, false);
        //DBHelper dbHelper = DBHelper.getHelper(this.getActivity());
        partsDao = new PartsDao(this.getActivity());
        parts = partsDao.getParents();
        BoardDao boardDao = new BoardDao(this.getActivity());
        YearDao yearDao = new YearDao(this.getActivity());
        boards = boardDao.getResult();
        years = yearDao.getResult();
        Print.e(this, "total parts size: "+parts.size());
        CustomGridAdapter adapter = new CustomGridAdapter(this.getActivity(), parts);
        grid=(GridView)mView.findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Print.e(this, "Position: "+position + "title: "+ parts.get(position).getTitle());
                Print.e(this, "selected id: "+parts.get(position).getId());
                ArrayList<Result> childs = partsDao.getChilds(parts.get(position).getId());
                if (childs == null || childs.size() <= 0){
                    Print.e(this, "no childs");
                }else{
                    Print.e(this, "child count: "+childs.size()+" :"+childs.get(0).getTitle());
                }
               /* if(parts.get(position).getTag()!=null){
                    if(parts.get(position).getTag().equals(CommonConstants.TAG_PBQ)){
                        setPBQDialog(position);
                    }else if(parts.get(position).getTag().equals(CommonConstants.TAG_SUGGESTION)){
                        setSuggestionDialog(position);
                    }else{
                        Intent intent = new Intent(HomeFragment.this.getActivity(), ParticipationActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(CommonConstants.EXTRA_PART_ID, parts.get(position).getId()+"");
                        intent.putExtra(CommonConstants.EXTRA_TAG, parts.get(position).getTag()+"");
                        intent.putExtra(CommonConstants.EXTRA_TITLE, parts.get(position).getTitle()+"");

                        startActivity(intent);
                    }
                } else{
                    Print.e(this, "tag null");
                }*/

            }
        });
        startGridAnimation();
        //or
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(grid!= null){
            startGridAnimation();
        }
    }
    public void startGridAnimation(){
        AnimationSet set = new AnimationSet(true);
        int duration = 400;
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(duration);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f,Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(duration);
        set.addAnimation(animation);

        LayoutAnimationController controller =
                new LayoutAnimationController(set, 0.5f);
        grid.setLayoutAnimation(controller);

        /*Animation animation = AnimationUtils.loadAnimation(this,R.anim.scale_center);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        grid.setLayoutAnimation(controller);*/
    }
    private void setPBQDialog(final int position){
        new YearBoardSpinnerDialog.Builder(SubHomeFragment.this.getActivity())
                .setTitle("")
                .setMessage("")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground("#FF4081")
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground("#FFA9A7A8")
                .isCancellable(true)
                .setBoards(boards)
                .setYears(years)
                .OnPositiveClicked(new YearBoardDialogListener() {
                    @Override
                    public void OnClick(Long boardId, String year) {
                        Intent intent = new Intent(SubHomeFragment.this.getActivity(), ParticipationActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(CommonConstants.EXTRA_PART_ID, parts.get(position).getId()+"");
                        intent.putExtra(CommonConstants.EXTRA_TAG, parts.get(position).getTag()+"");
                        intent.putExtra(CommonConstants.EXTRA_BOARD_ID, boardId+"");
                        intent.putExtra(CommonConstants.EXTRA_YEAR, year+"");
                        intent.putExtra(CommonConstants.EXTRA_TITLE, parts.get(position).getTitle()+"");
                        startActivity(intent);
                    }
                })
                .OnNegativeClicked(new YearBoardDialogListener() {
                    @Override
                    public void OnClick(Long boardId, String year) {
                    }
                })
                .build();
    }

    private void setSuggestionDialog(final int position){
        new YearBoardSpinnerDialog.Builder(SubHomeFragment.this.getActivity())
                .setTitle("")
                .setMessage("")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground("#FF4081")
                .setPositiveBtnText("Ok")
                .setBoards(boards)
                .setNegativeBtnBackground("#FFA9A7A8")
                .isCancellable(true)
                .OnPositiveClicked(new YearBoardDialogListener() {
                    @Override
                    public void OnClick(Long boardId, String year) {
                        Intent intent = new Intent(SubHomeFragment.this.getActivity(), ParticipationActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(CommonConstants.EXTRA_PART_ID, parts.get(position).getId()+"");
                        intent.putExtra(CommonConstants.EXTRA_TAG, parts.get(position).getTag()+"");
                        intent.putExtra(CommonConstants.EXTRA_YEAR, year+"");
                        intent.putExtra(CommonConstants.EXTRA_TITLE, parts.get(position).getTitle()+"");

                        gotPartActivity(intent);
                    }
                })
                .OnNegativeClicked(new YearBoardDialogListener() {
                    @Override
                    public void OnClick(Long boardId, String year) {

                    }
                })
                .build();
    }

    private void gotPartActivity(Intent intent){
        startActivity(intent);
    }
}
