/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.iwd.testpaper.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iwd.testpaper.R;
import com.iwd.testpaper.activity.DistinctExamHistoryActivity;
import com.iwd.testpaper.activity.ExamActivity;
import com.iwd.testpaper.activity.ParticipationActivity;
import com.iwd.testpaper.activity.SignUpActivity;
import com.iwd.testpaper.circletextview.CircleTextView;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.common.CommonSettings;
import com.iwd.testpaper.customdialog.ResolutionDialog;
import com.iwd.testpaper.customdialog.ResolutionListnear;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.db.dao.UserDao;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.handler.ApiCallManager;

import com.iwd.testpaper.retrofit.responces.examhistory.Result;
import com.iwd.testpaper.utility.ConnectionDetector;
import com.iwd.testpaper.utility.DividerItemDecoration;
import com.iwd.testpaper.utility.Print;

import java.util.List;

import spruce.Spruce;
import spruce.animation.DefaultAnimations;
import spruce.sort.DefaultSort;


public class ExamHistoryFragment extends Fragment {

    public static ExamHistoryFragment newInstance() {
        return new ExamHistoryFragment();
    }

    private RecyclerView recyclerView;
    private Animator spruceAnimator;
    Bundle bundle;
    ApiCallManager apiCallManager;
    private FragmentManager fm = null;
    private NoNet mNoNet;




    List<Result> results;
    String toolBarTitle;
    UserDao userDao;
    ExamHistoriyDao examHistoriyDao;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) container.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        apiCallManager = ApiCallManager.getInstance(ExamHistoryFragment.this.getActivity(),
                getString(R.string.base_url), CommonSettings.getInstance(this.getActivity()).getApiToken());

        userDao = new UserDao(this.getActivity());
        bundle = getArguments();
        if (bundle != null){
            if(bundle.containsKey(CommonConstants.EXTRA_TITLE)){
                toolBarTitle = bundle.getString(CommonConstants.EXTRA_TITLE);
            }

        }

        examHistoriyDao = new ExamHistoriyDao(this.getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                initSpruce();
            }
        };
        results = examHistoriyDao.getResult(CommonSettings.getInstance(this.getActivity()).getUserId());
        if (results.size()> 0){
            recyclerView.setAdapter(new RecyclerAdapter(results));
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            recyclerView.setVisibility(View.VISIBLE);
            container.findViewById(R.id.tvNoContentFound).setVisibility(View.GONE);
        }else{
           container.findViewById(R.id.tvNoContentFound).setVisibility(View.VISIBLE);
           recyclerView.setVisibility(View.GONE);
        }

        //or
        return inflater.inflate(R.layout.activity_part, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (spruceAnimator != null) {
            spruceAnimator.start();
        }
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }
        if(toolBarTitle != null){
            ((ParticipationActivity)this.getActivity()).setActionBarTitle(toolBarTitle);
        }
    }

    private void initSpruce() {
        spruceAnimator = new Spruce.SpruceBuilder(recyclerView)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(recyclerView, 800),
                        ObjectAnimator.ofFloat(recyclerView, "translationX", -recyclerView.getWidth(), 0f).setDuration(800))
                .start();
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<Result> results;

        class ViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout placeholderView;

            TextView tvContent;
            Long id;
            ViewHolder(View itemView) {
                super(itemView);
                placeholderView = (RelativeLayout) itemView.findViewById(R.id.placeholder_view);
                tvContent = (TextView) itemView.findViewById(R.id.tvContent);
            }
            ResolutionDialog.Builder resolutionBuilder;
        }

        RecyclerAdapter(List<Result> results) {
            this.results = results;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.placeholder_exam_history, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {


            holder.tvContent.setText(results.get(position).getTitle()+"");

            holder.id = results.get(position).getId();
            holder.placeholderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ExamHistoryFragment.this.getActivity(), DistinctExamHistoryActivity.class);
                    intent.putExtra(CommonConstants.EXTRA_EXAM_HISTORY_ID,  holder.id );
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    ExamHistoryFragment.this.getActivity().startActivity(intent);
                   // ExamHistoryFragment.this.getActivity().finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return results.size();
        }

    }

    public void gotoExamActivity(String title, int time, long participationId){
        Intent intent = new Intent(ExamHistoryFragment.this.getActivity(),
                ExamActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        bundle.putString(CommonConstants.EXTRA_EXAM_TITLE, title);
        bundle.putInt(CommonConstants.EXTRA_EXAM_TIME, time);
        bundle.putLong(CommonConstants.EXTRA_PARTICIPATION_ID, participationId);
        Print.e(this, "particiption id: "+participationId);
       /* if (((ParticipationActivity)this.getActivity()).getBoardId() != null){
            bundle.putString(CommonConstants.EXTRA_BOARD_ID, ((ParticipationActivity)this.getActivity()).getBoardId());
        }
        if (((ParticipationActivity)this.getActivity()).getYear() != null){
            bundle.putString(CommonConstants.EXTRA_YEAR, ((ParticipationActivity)this.getActivity()).getYear());

        }
        if (((ParticipationActivity)this.getActivity()).getUserId() != null){
            bundle.putString(CommonConstants.EXTRA_USER_ID, ((ParticipationActivity)this.getActivity()).getUserId());

        }*/
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
