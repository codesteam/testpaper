/*
 *     Spruce
 *
 *     Copyright (c) 2017 WillowTree, Inc.
 *     Permission is hereby granted, free of charge, to any person obtaining a copy
 *     of this software and associated documentation files (the "Software"), to deal
 *     in the Software without restriction, including without limitation the rights
 *     to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *     copies of the Software, and to permit persons to whom the Software is
 *     furnished to do so, subject to the following conditions:
 *     The above copyright notice and this permission notice shall be included in
 *     all copies or substantial portions of the Software.
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *     IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *     FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *     LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *     OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *     THE SOFTWARE.
 *
 */

package com.iwd.testpaper.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.iwd.testpaper.R;
import com.iwd.testpaper.activity.DistinctExamHistoryActivity;
import com.iwd.testpaper.activity.ExamActivity;
import com.iwd.testpaper.activity.ParticipationActivity;
import com.iwd.testpaper.activity.SignUpActivity;
import com.iwd.testpaper.circletextview.CircleTextView;
import com.iwd.testpaper.common.CommonConstants;
import com.iwd.testpaper.common.CommonSettings;
import com.iwd.testpaper.customdialog.ResolutionDialog;
import com.iwd.testpaper.customdialog.ResolutionListnear;
import com.iwd.testpaper.db.dao.ExamHistoriyDao;
import com.iwd.testpaper.db.dao.ParticipationDao;
import com.iwd.testpaper.db.dao.QuestionSetDao;
import com.iwd.testpaper.db.dao.UserDao;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.ConnectionCallback;
import com.iwd.testpaper.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.iwd.testpaper.retrofit.api.APIClientResponse;
import com.iwd.testpaper.retrofit.handler.ApiCallManager;
import com.iwd.testpaper.retrofit.responces.participation.Result;
import com.iwd.testpaper.utility.ConnectionDetector;
import com.iwd.testpaper.utility.DividerItemDecoration;
import com.iwd.testpaper.utility.Print;

import java.util.List;

import spruce.Spruce;
import spruce.animation.DefaultAnimations;
import spruce.sort.DefaultSort;


public class ParticipationFragment extends Fragment {

    public static ParticipationFragment newInstance() {
        return new ParticipationFragment();
    }

    private RecyclerView recyclerView;
    private Animator spruceAnimator;
    Bundle bundle;
    ApiCallManager apiCallManager;
    private FragmentManager fm = null;
    private NoNet mNoNet;
    String partId;
    Long participationChildId = null;
    Long participationId = null;
    ParticipationDao participationDao;
    QuestionSetDao questionSetDao;
    List<Result> particaptions;
    String toolBarTitle;
    UserDao userDao;
    ExamHistoriyDao examHistoriyDao;
    boolean isQuestionSet = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) container.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        apiCallManager = ApiCallManager.getInstance(ParticipationFragment.this.getActivity(),
                getString(R.string.base_url), CommonSettings.getInstance(this.getActivity()).getApiToken());

        userDao = new UserDao(this.getActivity());
        bundle = getArguments();
        if (bundle != null){
            partId = bundle.getString(CommonConstants.EXTRA_PART_ID);
            Print.e(this, "Part id bundle:"+partId);
            if(bundle.containsKey(CommonConstants.EXTRA_PARTICIPATION_CHILD_ID)){
                participationChildId = bundle.getLong(CommonConstants.EXTRA_PARTICIPATION_CHILD_ID);
            }
            if(bundle.containsKey(CommonConstants.EXTRA_TITLE)){
                toolBarTitle = bundle.getString(CommonConstants.EXTRA_TITLE);
            }
            if (bundle.containsKey(CommonConstants.EXTRA_QUESTION_SET)){
                isQuestionSet = bundle.getBoolean(CommonConstants.EXTRA_QUESTION_SET);
            }
            if(bundle.containsKey(CommonConstants.EXTRA_PARTICIPATION_ID)){
                participationId = bundle.getLong(CommonConstants.EXTRA_PARTICIPATION_ID);
            }
        }
        participationDao = new ParticipationDao(this.getActivity());
        questionSetDao = new QuestionSetDao(this.getActivity());
        examHistoriyDao = new ExamHistoriyDao(this.getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                initSpruce();
            }
        };
        Print.e(this, "isQuestionSet: "+ isQuestionSet);
        if (!isQuestionSet){
            if (participationChildId != null){
                particaptions = participationDao.getChilds(participationChildId);
            }else{
                particaptions = participationDao.getParents(partId);
            }
        }else{
            particaptions = questionSetDao.getQsetAsParticaption(participationId);
        }
        if (particaptions.size() > 0){
            recyclerView.setAdapter(new RecyclerAdapter(particaptions));
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            container.findViewById(R.id.tvNoContentFound).setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else{
            container.findViewById(R.id.tvNoContentFound).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        //or
        return inflater.inflate(R.layout.activity_part, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (spruceAnimator != null) {
            spruceAnimator.start();
        }
        if(mNoNet!= null){
            if(mNoNet.isConnectionActive() && mNoNet.isRegistered()){
                mNoNet.hideDefaultDialog();
                mNoNet.unRegisterNoNet();
            }
        }
        if(toolBarTitle != null){
            ((ParticipationActivity)this.getActivity()).setActionBarTitle(toolBarTitle);
        }
    }

    private void initSpruce() {
        spruceAnimator = new Spruce.SpruceBuilder(recyclerView)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(recyclerView, 800),
                        ObjectAnimator.ofFloat(recyclerView, "translationX", -recyclerView.getWidth(), 0f).setDuration(800))
                .start();
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

        List<Result> results;

        class ViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout placeholderView;
            CircleTextView mCircleTextView;
            TextView tvContent;
            Long id;
            ImageView ivFree;
            ViewHolder(View itemView) {
                super(itemView);
                placeholderView = (RelativeLayout) itemView.findViewById(R.id.placeholder_view);
                mCircleTextView = (CircleTextView) itemView.findViewById(R.id.tvCircle);
                tvContent = (TextView) itemView.findViewById(R.id.tvContent);
                ivFree = (ImageView) itemView.findViewById(R.id.ivIconFree);
            }
            ResolutionDialog.Builder resolutionBuilder;
        }

        RecyclerAdapter(List<Result> results) {
            this.results = results;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.placeholder_recycler, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if (results.get(position).getFirstLetter()==null){
                holder.mCircleTextView.setText("T");
            }else{
                holder.mCircleTextView.setText(results.get(position).getFirstLetter()+"");
            }
            if (isQuestionSet){
                if (results.get(position).getPrice().equals(CommonConstants.DEFAULT_PRICE)){
                    holder.ivFree.setVisibility(View.VISIBLE);
                }else{
                    holder.ivFree.setVisibility(View.GONE);
                }
            }else{
                holder.ivFree.setVisibility(View.GONE);
            }

            holder.tvContent.setText(results.get(position).getTitle()+"");
           /* holder.mCircleTextView.setTextColor(getResources().getColor(R.color.red));
            holder.mCircleTextView.setSolidColor(getResources().getColor(R.color.green));*/
            holder.mCircleTextView.setStrokeWidth(0);
            holder.id = results.get(position).getId();
            Print.e(this, "participition id: "+results.get(position).getId());
            holder.ivFree.setColorFilter(getResources().getColor(R.color.free_icon), PorterDuff.Mode.MULTIPLY);
            holder.placeholderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isQuestionSet){
                        if(!results.get(position).getPrice().equals(CommonConstants.DEFAULT_PRICE)){ // paid exam_item
                            if(CommonSettings.getInstance(ParticipationFragment.this.getActivity()).getUserId()
                                    != CommonConstants.GHUEST_USER_ID){ // check already registered user
                                float balance = 0.0f;
                                try {
                                    balance = Float.parseFloat(userDao.getUser(CommonSettings.getInstance(ParticipationFragment.this.getActivity()).getUserId()).getBalance());
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                                float requriedAmount = 0.0f;
                                try {
                                    requriedAmount = Float.parseFloat(results.get(position).getPrice());
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                                if(examHistoriyDao.historyCount(CommonSettings.getInstance(ParticipationFragment.this.getActivity()).getUserId(), results.get(position).getId()) > 0){
                                   final ResolutionDialog.Builder resolutionBuilder = new ResolutionDialog.Builder(ParticipationFragment.this.getActivity());
                                    resolutionBuilder.isCancellable(false)
                                            .setRightBtnText(getString(R.string.exam_history))
                                            .setLeftBtnText("Back")
                                            .setMessage(getString(R.string.you_already_participted_in_this_exam))
                                            .build(new ResolutionListnear() {
                                                @Override
                                                public void onClickRight() {

                                                    Intent intent = new Intent(ParticipationFragment.this.getActivity(), DistinctExamHistoryActivity.class);
                                                    intent.putExtra(CommonConstants.EXTRA_EXAM_HISTORY_ID,  results.get(position).getId());
                                                    //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                                    ParticipationFragment.this.getActivity().startActivity(intent);
                                                    ParticipationFragment.this.getActivity().finish();

                                                }

                                                @Override
                                                public void onClickLeft() {
                                                    resolutionBuilder.dismiss();
                                                }
                                            });

                                }
                                else if (balance > requriedAmount){
                                    holder.resolutionBuilder = new ResolutionDialog.Builder(ParticipationFragment.this.getActivity());
                                    holder.resolutionBuilder.isCancellable(false)
                                            .setRightBtnText("OK")
                                            .setLeftBtnText("Back")
                                            .setMessage(getString(R.string.balance_debited_dialog_first_part)+" "+requriedAmount+" Tk. "+getString(R.string.balance_debited_dialog_second_part))
                                            .build(new ResolutionListnear() {
                                                @Override
                                                public void onClickRight() {
                                                    holder.resolutionBuilder.dismiss();
                                                    if (!ConnectionDetector.isNetworkPresent(ParticipationFragment.this.getActivity())){
                                                        fm = ParticipationFragment.this.getActivity().getSupportFragmentManager();
                                                        mNoNet = new NoNet(ParticipationFragment.this.getActivity());
                                                        mNoNet.initNoNet(R.layout.no_net_default_with_back, new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                mNoNet.hideDefaultDialog();
                                                            }
                                                        }, ParticipationFragment.this.getActivity(), fm, false);
                                                        mNoNet.setConnectionCallback(new ConnectionCallback() {
                                                            @Override
                                                            public void Networkupdate(boolean isConnectionActive) {
                                                                if(isConnectionActive){
                                                                    callForPurchase(CommonSettings.getInstance(ParticipationFragment.this.getActivity()).getUserId()+"", results.get(position).getId(), holder.tvContent.getText().toString(), results.get(position).getTime().intValue());
                                                                }
                                                            }
                                                        });
                                                        mNoNet.RegisterNoNet();
                                                        mNoNet.showDefaultDialog();


                                                    }else{
                                                        callForPurchase(CommonSettings.getInstance(ParticipationFragment.this.getActivity()).getUserId()+"",results.get(position).getId(), holder.tvContent.getText().toString(), results.get(position).getTime().intValue());
                                                    }
                                                    //got to payment screen and may be wait for result

                                                }

                                                @Override
                                                public void onClickLeft() {
                                                    holder.resolutionBuilder.dismiss();
                                                }
                                            });
                                }else{
                                    holder.resolutionBuilder = new ResolutionDialog.Builder(ParticipationFragment.this.getActivity());
                                    holder.resolutionBuilder.isCancellable(false)
                                            .setRightBtnText("Payment")
                                            .setLeftBtnText("Back")
                                            .setMessage(getString(R.string.go_to_payment_screen))
                                            .build(new ResolutionListnear() {
                                                @Override
                                                public void onClickRight() {
                                                    holder.resolutionBuilder.dismiss();
                                                    //got to payment screen and may be wait for result
                                                    //after all action like recharge and pay, return back to this activity
                                                    //and automatically send user to Exam activity

                                                }

                                                @Override
                                                public void onClickLeft() {
                                                    holder.resolutionBuilder.dismiss();
                                                }
                                            });
                                    //show dialog for payment screen
                                }
                            }else{//unrgistred guest user want to see paid stuff go to registration page
                                holder.resolutionBuilder = new ResolutionDialog.Builder(ParticipationFragment.this.getActivity());
                                holder.resolutionBuilder.isCancellable(false)
                                        .setRightBtnText("OK")
                                        .setLeftBtnText("Back")
                                        .setMessage(getString(R.string.you_must_have_registraion_for_exam))
                                        .build(new ResolutionListnear() {
                                            @Override
                                            public void onClickRight() {
                                                Intent i=new Intent(ParticipationFragment.this.getActivity(),SignUpActivity.class);
                                                i.putExtra(CommonConstants.EXTRA_FROM_PARTICIPTION_ACTIVITY, true);
                                                ParticipationFragment.this.getActivity().startActivity(i);
                                                holder.resolutionBuilder.dismiss();
                                            }

                                            @Override
                                            public void onClickLeft() {
                                                holder.resolutionBuilder.dismiss();
                                            }
                                        });

                            }
                        }else{ // free exam_item
                            gotoExamActivity( holder.tvContent.getText().toString(), results.get(position).getTime().intValue(),  results.get(position).getId());
                        }
                    }else{
                        if (participationDao.childCount(results.get(position).getId()+"") > 0){ // check for childs
                            Bundle bundle=new Bundle();
                            bundle.putLong(CommonConstants.EXTRA_PARTICIPATION_CHILD_ID, particaptions.get(position).getId());
                            bundle.putString(CommonConstants.EXTRA_TITLE, particaptions.get(position).getTitle());
                            ParticipationFragment fragment = new ParticipationFragment();
                            fragment.setArguments(bundle);
                            FragmentManager fragmentManager = ParticipationFragment.this.getActivity().getSupportFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.fragment_part, fragment, fragment.getClass().getName())
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                        }else{
                            Bundle bundle=new Bundle();
                            bundle.putLong(CommonConstants.EXTRA_PARTICIPATION_ID, particaptions.get(position).getId());
                            bundle.putString(CommonConstants.EXTRA_TITLE, particaptions.get(position).getTitle());
                            bundle.putBoolean(CommonConstants.EXTRA_QUESTION_SET, true);
                            ParticipationFragment fragment = new ParticipationFragment();
                            fragment.setArguments(bundle);
                            FragmentManager fragmentManager = ParticipationFragment.this.getActivity().getSupportFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.fragment_part, fragment, fragment.getClass().getName())
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                        }

                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return results.size();
        }

    }

    public void gotoExamActivity(String title, int time, long participationId){
        Intent intent = new Intent(ParticipationFragment.this.getActivity(),
                ExamActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        bundle.putString(CommonConstants.EXTRA_EXAM_TITLE, title);
        bundle.putInt(CommonConstants.EXTRA_EXAM_TIME, time);
        bundle.putLong(CommonConstants.EXTRA_PARTICIPATION_ID, participationId);
        Print.e(this, "particiption id: "+participationId);
       /* if (((ParticipationActivity)this.getActivity()).getBoardId() != null){
            bundle.putString(CommonConstants.EXTRA_BOARD_ID, ((ParticipationActivity)this.getActivity()).getBoardId());
        }
        if (((ParticipationActivity)this.getActivity()).getYear() != null){
            bundle.putString(CommonConstants.EXTRA_YEAR, ((ParticipationActivity)this.getActivity()).getYear());

        }
        if (((ParticipationActivity)this.getActivity()).getUserId() != null){
            bundle.putString(CommonConstants.EXTRA_USER_ID, ((ParticipationActivity)this.getActivity()).getUserId());

        }*/
        intent.putExtras(bundle);
        startActivity(intent);
    }
    String userId = "user";
    private void callForPurchase(String userId, final Long participtionId, final String productTitle, final int time){

        apiCallManager.callForPurchase(null, String.valueOf(participtionId), userId, new APIClientResponse() {
            @Override
            public void onFailure(String msg, Object sender) {
                gotoExamActivity( productTitle, time, participtionId);
            }

            @Override
            public void onSuccess(String msg, Object sender) {
                gotoExamActivity( productTitle, time, participtionId);
            }
        });
    }
}
