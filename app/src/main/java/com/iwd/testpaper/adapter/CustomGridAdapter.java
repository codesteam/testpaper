package com.iwd.testpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.iwd.testpaper.R;
import com.iwd.testpaper.retrofit.responces.parts.Result;


import java.util.ArrayList;

public class CustomGridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Result> results;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.warning_gray)
            .error(R.drawable.warning_gray)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH);
    public CustomGridAdapter(Context c,ArrayList<Result> results ) {
        mContext = c;
        this.results = results;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return results.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_single, null);
           // Glide.with(mContext).load(results.get(position).getImageUrl()).apply(options).into(imageView);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.grid_text);
        ImageView imageView = (ImageView)convertView.findViewById(R.id.grid_image);
        textView.setText(results.get(position).getTitle());

        Glide.with(mContext)
                .applyDefaultRequestOptions(options)
                .load(results.get(position).getImage())
                .into(imageView);

        return convertView;
    }



}