package com.iwd.testpaper.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.iwd.testpaper.R;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class ResolutionDialog {


    private YearBoardDialogListener pListener,nListener;
    private boolean cancel;


    private ResolutionDialog(Builder builder){
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String title,message;
        private Activity activity;
        Button btnLeft;
        Button btnRight;
        TextView tvMessage;
        String leftBtnText;
        String rightBtnText;

        private boolean cancel;
        Dialog dialog;
        public Builder(Activity activity){
            this.activity=activity;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }

        public Builder setLeftBtnText(String leftBtnText){
            this.leftBtnText = leftBtnText;
            return this;
        }
        public Builder setRightBtnText(String rightBtnText){
            this.rightBtnText = rightBtnText;
            return this;
        }
        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public ResolutionDialog build(final ResolutionListnear resolutionListnear){
            dialog=new Dialog(activity);
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setTitle("WARNING!!!");
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_resolution);
            dialog.show();
            btnLeft = dialog.findViewById(R.id.btnLeft);
            btnRight = dialog.findViewById(R.id.btnRight);
            btnLeft.setText(leftBtnText);
            btnRight.setText(rightBtnText);
            tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
            tvMessage.setText(message);
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 resolutionListnear.onClickLeft();
                }
            });
            btnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resolutionListnear.onClickRight();
                }
            });

            return new ResolutionDialog(this);

        }

        public void dismiss(){
            if(dialog != null){
                dialog.dismiss();
            }
        }

    }

}
