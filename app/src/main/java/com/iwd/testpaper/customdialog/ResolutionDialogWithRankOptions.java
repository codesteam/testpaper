package com.iwd.testpaper.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.iwd.testpaper.R;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class ResolutionDialogWithRankOptions {


    private YearBoardDialogListener pListener,nListener;
    private boolean cancel;


    private ResolutionDialogWithRankOptions(Builder builder){
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String title,message;
        private Activity activity;

        TextView tvMessage;;
        private boolean cancel;
        Dialog dialog;
        public Builder(Activity activity){
            this.activity=activity;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }

        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public ResolutionDialogWithRankOptions build(final RankListnear rankListnear){
            dialog=new Dialog(activity);
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_resolution_with_options);
            dialog.show();
            dialog.findViewById(R.id.btnSkip).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rankListnear.rank(0);
                }
            });
            ((RadioGroup)dialog.findViewById(R.id.rg1)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (checkedId == R.id.excellent) {
                        rankListnear.rank(5);
                    } else  if (checkedId == R.id.good) {
                        rankListnear.rank(4);
                    } else  if (checkedId == R.id.fair) {
                        rankListnear.rank(3);
                    }else  if (checkedId == R.id.poor) {
                        rankListnear.rank(2);
                    }else  if (checkedId == R.id.very_bad) {
                        rankListnear.rank(1);
                    }


                }
            });
            tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);

            return new ResolutionDialogWithRankOptions(this);

        }

        public void dismiss(){
            if(dialog != null){
                dialog.dismiss();
            }
        }

    }

}
