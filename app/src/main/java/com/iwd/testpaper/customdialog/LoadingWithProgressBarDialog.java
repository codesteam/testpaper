package com.iwd.testpaper.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iwd.testpaper.R;
import com.iwd.testpaper.wang.avi.AVLoadingIndicatorView;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class LoadingWithProgressBarDialog {

    private boolean cancel;


    private LoadingWithProgressBarDialog(Builder builder){
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String title,message;
        TextView tvLoading;
        private Activity activity;
        private AVLoadingIndicatorView avi;
        private boolean cancel;
        Dialog dialog;
        ProgressBar pb;
        RelativeLayout rlBtns;
        LinearLayout llImageProgress;
        ResolutionListnear resolutionListnear;
        ImageView imageViewPb;
        public Builder(Activity activity, ResolutionListnear resolutionListnear){
            this.activity=activity;
            this.resolutionListnear = resolutionListnear;
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }

        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public LoadingWithProgressBarDialog build(){
            dialog=new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_loading_with_progressbar);
            dialog.show();
            avi= (AVLoadingIndicatorView) dialog.findViewById(R.id.avi);
            avi.setIndicator("LineScalePulseOutRapidIndicator");
            tvLoading = (TextView) dialog.findViewById(R.id.graduallyTextView);
            pb = (ProgressBar) dialog.findViewById(R.id.pb);
            rlBtns = (RelativeLayout) dialog.findViewById(R.id.barBtn);
            llImageProgress = (LinearLayout) dialog.findViewById(R.id.llImage);
            imageViewPb = (ImageView)dialog.findViewById(R.id.ivPb) ;
            dialog.findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    modeAVI();
                    resolutionListnear.onClickRight();
                }
            });
            dialog.findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resolutionListnear.onClickLeft();
                }
            });
            modeAVI();
            //tvLoading.startLoading();
           /* if (!TextUtils.isEmpty("text")) {
                tvLoading.setText(text);
            }*/

            return new LoadingWithProgressBarDialog(this);

        }

        public void modeAVI(){
            llImageProgress.setVisibility(View.GONE);
            avi.setVisibility(View.VISIBLE);
            avi.startAnimation();
            rlBtns.setVisibility(View.GONE);
            tvLoading.setText(activity.getString(R.string.loading_upper_case_only));
        }
        public void modePB(){
            llImageProgress.setVisibility(View.VISIBLE);
            avi.setVisibility(View.GONE);
            rlBtns.setVisibility(View.GONE);
            tvLoading.setText(activity.getString(R.string.loading_upper_case_only));
        }

        public ProgressBar getPb() {
            return pb;
        }
        public ImageView getImageViewPb(){
            return imageViewPb;
        }

        public void modeRetry(){
            rlBtns.setVisibility(View.VISIBLE);
            avi.setVisibility(View.GONE);
            llImageProgress.setVisibility(View.GONE);
            tvLoading.setText(activity.getString(R.string.internet_problem_try_again_later));
        }

        public void dismiss(){
            if(dialog != null){
                dialog.dismiss();
            }
            if (tvLoading != null){
               // tvLoading.stopLoading();
            }
        }

    }

}
