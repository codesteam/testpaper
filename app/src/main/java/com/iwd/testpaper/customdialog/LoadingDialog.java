package com.iwd.testpaper.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.TextView;


import com.iwd.testpaper.R;

import com.iwd.testpaper.wang.avi.AVLoadingIndicatorView;


/**
 * Created by Shashank Singhal on 06/01/2018.
 */

public class LoadingDialog {


    private YearBoardDialogListener pListener,nListener;
    private boolean cancel;


    private LoadingDialog(Builder builder){
        this.cancel=builder.cancel;
    }


    public static class Builder{
        private String message = "LO";
        TextView mGraduallyTextView;
        private String title;
        private Activity activity;
        private AVLoadingIndicatorView avi;
        private boolean cancel;
        Dialog dialog;
        public Builder(Activity activity){
            this.activity=activity;
            message = this.activity.getString(R.string.loading_upper_case_only);
        }
        public Builder setTitle(String title){
            this.title=title;
            return this;
        }

        public Builder setMessage(String message){
            this.message=message;
            return this;
        }

        public Builder isCancellable(boolean cancel){
            this.cancel=cancel;
            return this;
        }

        public LoadingDialog build(){
            dialog=new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(cancel);
            dialog.setContentView(R.layout.dialog_loading);
            dialog.show();
            avi= (AVLoadingIndicatorView) dialog.findViewById(R.id.avi);
            avi.setIndicator("LineScalePulseOutRapidIndicator");
            mGraduallyTextView = (TextView) dialog.findViewById(R.id.graduallyTextView);
            mGraduallyTextView.setText(message);
            return new LoadingDialog(this);

        }

        public void dismiss(){
            if(dialog != null){
                dialog.dismiss();
            }
        }

    }

}
