package com.iwd.testpaper.corouselpagerkk;
/*
 * 
 * Copyright (C) 2014 Krishna Kumar Sharma
 * 
 *  */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.iwd.testpaper.R;
import com.iwd.testpaper.activity.ExamActivity;
import com.iwd.testpaper.retrofit.responces.question.Question;
import com.iwd.testpaper.retrofit.responces.question.Result;
import com.iwd.testpaper.utility.ToastMsg;

public final class  TestFragment extends Fragment {

    private TextView title;
    private RelativeLayout card;
    Activity context;
    AppCompatButton btnSkip, btnSubmit, btnNext;
    boolean lastItem = false;
    ImageView ivQuestion;
    TextView tvQuestion;
    RadioGroup radioGroup;
    RadioButton rb1,rb2,rb3,rb4;
    TestFragmentAdapter testFragmentAdapter;
    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.warning_gray)
            .error(R.drawable.warning_gray)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH);

    public static TestFragment newInstance(Result content, Activity c, boolean lastItem, TestFragmentAdapter testFragmentAdapter) {

        TestFragment fragment = new TestFragment();
        fragment.lastItem = lastItem;
        fragment.mContent = content;
        fragment.context=c;
        fragment.testFragmentAdapter = testFragmentAdapter;
        return fragment;
    }

    private Result mContent;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootview = (ViewGroup) inflater.inflate(R.layout.exam_item,
                container, false);
        btnNext = (AppCompatButton) rootview.findViewById(R.id.btnNext);
        btnSkip = (AppCompatButton) rootview.findViewById(R.id.btnSkip);
        btnSubmit = (AppCompatButton) rootview.findViewById(R.id.btnSubmit);
        tvQuestion = (TextView) rootview.findViewById(R.id.tvQuestion);
        ivQuestion = (ImageView) rootview.findViewById(R.id.ivQuestion);
        radioGroup = (RadioGroup) rootview.findViewById(R.id.rg1);
        rb1 = (RadioButton) rootview.findViewById(R.id.rb1);
        rb2 = (RadioButton) rootview.findViewById(R.id.rb2);
        rb3 = (RadioButton) rootview.findViewById(R.id.rb3);
        rb4 = (RadioButton) rootview.findViewById(R.id.rb4);
        if(mContent.getTitle()!=null){
            if(mContent.getTitle().length()>0){
                tvQuestion.setText(mContent.getTitle()+"");
            }else{
                tvQuestion.setVisibility(View.GONE);
            }
        }else{
            tvQuestion.setVisibility(View.GONE);
        }

        if(mContent.getOptions().size()<4){
            rb4.setVisibility(View.GONE);
        }
        if(mContent.getOptions().size()<3){
            rb3.setVisibility(View.GONE);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb1) {
                   mContent.setSelOptionId(mContent.getOptions().get(0).getId());
                } else  if (checkedId == R.id.rb2) {
                    mContent.setSelOptionId(mContent.getOptions().get(1).getId());
                } else  if (checkedId == R.id.rb3) {
                    mContent.setSelOptionId(mContent.getOptions().get(2).getId());
                }else  if (checkedId == R.id.rb4) {
                    mContent.setSelOptionId(mContent.getOptions().get(3).getId());
                }


            }
        });
        if (mContent.getImage() != null){
            if(mContent.getImage().length()>0){
                Glide.with(context)
                        .applyDefaultRequestOptions(options)
                        .load(mContent.getImage())
                        .into(ivQuestion);
            }else{
                ivQuestion.setVisibility(View.GONE);
            }

        }else{
            ivQuestion.setVisibility(View.GONE);
        }
        if (lastItem){
            btnNext.setVisibility(View.INVISIBLE);
            btnSkip.setVisibility(View.INVISIBLE);
            btnSubmit.setVisibility(View.VISIBLE);
        }else{
            btnSubmit.setVisibility(View.INVISIBLE);
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1)
                {
                    ToastMsg.Toast(TestFragment.this.getActivity(), getString(R.string.please_check_a_option));
                    // no radio buttons are checked
                }else{
                    testFragmentAdapter.getAnswers().add(mContent);
                    ((ExamActivity)context).moveToNextQuestion();
                    ((ExamActivity)context).setAnswerd(testFragmentAdapter.getAnswers().size());
                }

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExamActivity)context).moveToNextQuestion();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1)
                {
                    ToastMsg.Toast(TestFragment.this.getActivity(), getString(R.string.please_check_a_option));
                    // no radio buttons are checked
                }else{
                    testFragmentAdapter.getAnswers().add(mContent);
                    ((ExamActivity)context).setAnswerd(testFragmentAdapter.getAnswers().size());
                    ((ExamActivity)context).submitExam(testFragmentAdapter.getAnswers());
                }

            }
        });

        return rootview;
    }
}
