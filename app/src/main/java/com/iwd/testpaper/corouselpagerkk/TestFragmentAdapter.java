package com.iwd.testpaper.corouselpagerkk;
/*
 * 
 * Copyright (C) 2014 Krishna Kumar Sharma
 * 
 *  */

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.iwd.testpaper.retrofit.responces.question.Result;

import java.util.ArrayList;

public class TestFragmentAdapter extends FragmentStatePagerAdapter {

    private Activity context;
    private ArrayList<Result> questions;
    private ArrayList<Result> answers;

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Object obj = super.instantiateItem(container, position);
        return obj;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        if (object != null) {
            return ((Fragment) object).getView() == view;
        } else {
            return false;
        }
    }

    public TestFragmentAdapter(FragmentManager fm,
                               Activity context, ArrayList<Result> questions) {
        super(fm);
        this.context = context;
        this.questions = questions;
        answers = new ArrayList<>();
    }

    @Override
    public int getItemPosition(Object object) {
        // Causes adapter to reload all Fragments when
        // notifyDataSetChanged is called
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        boolean lastItem = false;
        if (position == questions.size() - 1){
            lastItem = true;
        }
        return TestFragment.newInstance(questions.get(position),
                context, lastItem, this);
    }

    @Override
    public int getCount() {
        return questions == null ? 0 : questions.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return questions.get(position).getTitle();
    }

    public void setAnswer(int pos, long id){
        questions.get(pos).setSelOptionId(id);
    }

    public ArrayList<Result> getAnswers() {
        return answers;
    }
}