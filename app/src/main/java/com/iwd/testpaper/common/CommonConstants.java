package com.iwd.testpaper.common;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.iwd.testpaper.activity.LoginActivity;
import com.iwd.testpaper.activity.MainActivity;
import com.iwd.testpaper.activity.MyAccountActivity;
import com.iwd.testpaper.activity.SignUpActivity;

public class CommonConstants {
    public static final int ERROR_CODE_EXAM_PARICIPITED = 403;
    public static final int ERROR_CODE_PAYMENT_REQUIRED = 402;

    public static final String TAG_SUGGESTION = "suggestion";
    public static final String TAG_PBQ = "pbq";
    public static final String TAG_MODEL_TEST = "model_test";
    public static final String TAG_DAILY_EXAM = "dally_exam";
    public static final String TAG_PACKAGE = "package";
    public static final String TAG_EXAM_HISTORY = "exam_history";
    public static final String TAG_NOTICE_BOARD = "notice";
    public static final String TAG_TEST_EXAM = "test_exam";

    public static final String EXTRA_TAG = "tag";
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_PART_ID = "partid";
    public static final String EXTRA_PART_CHILD_ID = "partchildid";
    public static final String EXTRA_PARTICIPATION_CHILD_ID = "participitionchildid";
    public static final String EXTRA_PARTICIPATION_ID = "participitionid";
    public static final String EXTRA_EXAM_HISTORY_ID = "examhistoryid";
    public static final String EXTRA_BOARD_ID = "boardid";
    public static final String EXTRA_EXAM_TIME = "time";
    public static final String EXTRA_YEAR = "extrayear";
    public static final String EXTRA_SUBJECT_ID = "subid";
    public static final String EXTRA_CHAPTER_ID = "chapid";
    public static final String EXTRA_USER_ID = "user_id";

    public static final String EXTRA_QUESTION_SET = "question_set";
    public static final String EXTRA_EXAM_TITLE = "exam_title";
    public static final String EXTRA_OPEN_SIGN_UP_DIALOG = "opensignupdialog";
    public static final String EXTRA_FROM_PARTICIPTION_ACTIVITY = "fromparticipitionactivity";
    public static final String EXTRA_FROM_SGNIN_ACTIVITY = "fromsigninactivity";
    public static final String EXTRA_TOOL_BAR_TITLE = "toolbartitle";

    public static final boolean dummy_data_active = false;
    public static final String KEY_IS_FIRST_ITEM="is_first_item";
    public static final String MAX_ENTRIES_FOR_API_CALL = "200";
    public static final String START_TIME_STAMP=    "1111";
    public static final String DEFAULT_PRICE="0.0";
    public static Long GHUEST_USER_ID = -1L;
    public static int REQUEST_CODE_FOR_DETAILS_ACTIVITY = 65;
    public static int DEFAULT_CAT_LIST_POSITION=0;  // for those case when we have to pick a category from entry itself
                                                    // since entry return list of categoris so pick the first index position
                                                    // e.g when user click on notification we have to find entry cat from catlist that entry has
                                                    // but we dont no for which category entry is pointing specifically
                                                    // so using index 0

    public static int DEFAULT_MIDIA_LIST_POSITION=0; // since there is media list (small list, large list, video list)
                                                    // which one we will sow for thumbnil, there is no selection specifically
                                                    // so using the index 0
    public static int DEFAULT_CAT_SEARCH_ID = 0;
    public static int DEFAULT_CAT_HOME_ID = 99;
    public static int MAX_NUMBER_OF_SPECIAL_NEWS = 3;
    public static int DEFAULT_SELECTED_DRAWER_ITME = 1;
    public static String NOT_FOUND = "404";
    public static int MIN_SDK_VERSION_FOR_SEARCH_VEIW = Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static int swipeRefreshLayoutProgressOfset = 2; // must be even number// this value multiplied with action bar size
    public static final int AD_WAIT_IN_SEC = 10;

    public static void NoHistoryIntent(Context packageContext, Class<?> cls) {
        Intent intent = new Intent(packageContext, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        packageContext.startActivity(intent);

    }
    public static void NoHistoryIntentFinishCurrent(Activity requester, Class<?> cls) {
        Intent intent = new Intent(requester, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        requester.startActivity(intent);
        requester.finish();

    }
    public static void GotoMainActivityFinishCurrent(Activity requester){
        Intent i=new Intent(requester,MainActivity.class);
        requester.startActivity(i);
        requester.finish();
    }
    public static void GotoLoginActivity(Activity requester){
        Intent i=new Intent(requester,LoginActivity.class);
        requester.startActivity(i);
        requester.finish();
    }
    public static String getFormatedPhoneNumber(String phone){
        String tempPhone = phone;
        if(!phone.contains("+88")){
            tempPhone = "+88"+phone;
        }
        if(!tempPhone.contains("+880")){
            tempPhone = "+880"+phone;
        }
        phone = tempPhone;
        return  phone;
    }
}
